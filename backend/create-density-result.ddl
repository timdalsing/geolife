create table density_result (
    waypoint_id varchar(40) primary key,
    cluster int not null,
    centroid_lng float not null,
    centroid_lat float not null
);

create index density_result_cluster_ndx on density_result (cluster);
