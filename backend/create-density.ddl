create table density_percentage (
    cluster int primary key,
    density_count bigint not null
);
