package dev.dalsing.geolife.mongo.consumer;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.mongo.common.MongoWaypoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.UUID;
import java.util.function.Consumer;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class MongoConsumerAppIntegrationTest {

    @Autowired
    MongoConsumerApp app;

    @Autowired
    ReactiveMongoTemplate template;

    static Random random = new Random();

    String id = UUID.randomUUID().toString();
    int personId = random.nextInt(1000);
    String trajectoryId = UUID.randomUUID().toString();
    float lat = random.nextFloat();
    float lng = random.nextFloat();
    int alt = random.nextInt(1000);
    LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

    GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
            .id(id)
            .personId(personId)
            .trajectoryId(trajectoryId)
            .lat(lat)
            .lng(lng)
            .altitude(alt)
            .timestamp(ts)
            .build();
    MongoWaypoint mongoWaypoint = MongoWaypoint.builder()
            .id(id)
            .personId(personId)
            .trajectoryId(trajectoryId)
            .lat(lat)
            .lng(lng)
            .altitude(alt)
            .timestamp(ts)
            .build();

    @BeforeEach
    void setUp() {
        template.remove(MongoWaypoint.class).all().subscribe();
    }

    @AfterEach
    void tearDown() {
        template.remove(MongoWaypoint.class).all().subscribe();
    }

    @Tag("integration")
    @Test
    void consumer() {
        Consumer<Flux<GeolifeWaypoint>> consumer = app.consumer();
        Flux<GeolifeWaypoint> waypointFlux = Flux.just(waypoint);
        consumer.accept(waypointFlux);
        Mono<MongoWaypoint> waypointMono = template.findById(id, MongoWaypoint.class);
        StepVerifier.create(waypointMono).assertNext(wp -> assertThat(wp).isEqualTo(mongoWaypoint)).verifyComplete();
    }
}