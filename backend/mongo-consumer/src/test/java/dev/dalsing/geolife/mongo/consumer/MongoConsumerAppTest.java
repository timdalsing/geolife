package dev.dalsing.geolife.mongo.consumer;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.mongo.common.MongoWaypoint;
import io.micrometer.core.instrument.Timer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.function.Consumer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MongoConsumerAppTest {

    MongoConsumerApp app;

    @Mock
    ReactiveMongoTemplate template;

    @Mock
    Timer timer;

    String id = "some-id";
    int personId = 3;
    String trajectoryId = "some-trajectory-id";
    float lat = 12.34f;
    float lng = 12.34f;
    int alt = 100;
    LocalDateTime ts = LocalDateTime.now();

    GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
            .id(id)
            .personId(personId)
            .trajectoryId(trajectoryId)
            .lat(lat)
            .lng(lng)
            .altitude(alt)
            .timestamp(ts)
            .build();
    MongoWaypoint mongoWaypoint = MongoWaypoint.builder()
            .id(id)
            .personId(personId)
            .trajectoryId(trajectoryId)
            .lat(lat)
            .lng(lng)
            .altitude(alt)
            .timestamp(ts)
            .build();

    @BeforeEach
    void setUp() {
        app = new MongoConsumerApp(template, timer);
    }

    @Test
    void consumer() {
        when(template.insert(any(MongoWaypoint.class))).thenReturn(Mono.just(mongoWaypoint));

        Consumer<Flux<GeolifeWaypoint>> consumer = app.consumer();

        Flux<GeolifeWaypoint> waypointFlux = Flux.just(waypoint);
        consumer.accept(waypointFlux);
        verify(template).insert(mongoWaypoint);
    }
}