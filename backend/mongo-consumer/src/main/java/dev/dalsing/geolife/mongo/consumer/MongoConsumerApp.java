package dev.dalsing.geolife.mongo.consumer;

import dev.dalsing.geolife.common.ObjectMapperConfig;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.mongo.common.MongoWaypoint;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import reactor.core.publisher.Flux;

import java.util.function.Consumer;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@Import({ ObjectMapperConfig.class})
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class MongoConsumerApp {

    ReactiveMongoTemplate template;
    Timer timer;

    public static void main(String[] args) {
        SpringApplication.run(MongoConsumerApp.class, args);
    }

    @Bean
    public static Timer timer(MeterRegistry registry) {
        return registry.timer("mongo-consumer-timer");
    }

    @Bean
    public Consumer<Flux<GeolifeWaypoint>> consumer() {
        return flux -> flux
                .map(waypoint -> MongoWaypoint.builder()
                        .id(waypoint.getId())
                        .personId(waypoint.getPersonId())
                        .trajectoryId(waypoint.getTrajectoryId())
                        .lat(waypoint.getLat())
                        .lng(waypoint.getLng())
                        .altitude(waypoint.getAltitude())
                        .timestamp(waypoint.getTimestamp())
                        .build())
                .subscribe(waypoint -> timer.record(() -> template.insert(waypoint).subscribe()));
    }
}
