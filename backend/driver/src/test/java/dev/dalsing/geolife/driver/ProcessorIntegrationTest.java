package dev.dalsing.geolife.driver;

import dev.dalsing.geolife.pb.PBWaypoint;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.MimeType;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@Slf4j
class ProcessorIntegrationTest {

    @Autowired
    Processor processor;

    @MockBean
    StreamBridgeWrapper bridge;

    @Captor
    ArgumentCaptor<PBWaypoint> waypointCaptor;

    @BeforeEach
    void setUp() {
    }

    @Tag("integration")
    @Test
    void run() {
        List<PBWaypoint> expected = List.of(
                PBWaypoint.newBuilder()
                        .setPersonId(0)
                        .setTrajectoryId("20081023025304")
                        .setTimestamp(1224730384000L)
                        .setAltitude(492)
                        .setLng(116.318417f)
                        .setLat(39.984702f)
                        .build(),
                PBWaypoint.newBuilder()
                        .setPersonId(0)
                        .setTrajectoryId("20081023025304")
                        .setTimestamp(1224730390000L)
                        .setAltitude(492)
                        .setLng(116.31845f)
                        .setLat(39.984683f)
                        .build(),
                PBWaypoint.newBuilder()
                        .setPersonId(0)
                        .setTrajectoryId("20081024020959")
                        .setTimestamp(1224814199000L)
                        .setAltitude(492)
                        .setLng(116.319876f)
                        .setLat(40.008305f)
                        .build(),
                PBWaypoint.newBuilder()
                        .setPersonId(0)
                        .setTrajectoryId("20081024020959")
                        .setTimestamp(1224814204000L)
                        .setAltitude(491)
                        .setLng(116.319962f)
                        .setLat(40.008413f)
                        .build(),
                PBWaypoint.newBuilder()
                        .setPersonId(1)
                        .setTrajectoryId("20081023055305")
                        .setTimestamp(1224741185000L)
                        .setAltitude(492)
                        .setLng(116.319236f)
                        .setLat(39.984094f)
                        .build(),
                PBWaypoint.newBuilder()
                        .setPersonId(1)
                        .setTrajectoryId("20081023055305")
                        .setTimestamp(1224741186000L)
                        .setAltitude(492)
                        .setLng(116.319322f)
                        .setLat(39.984198f)
                        .build(),
                PBWaypoint.newBuilder()
                        .setPersonId(1)
                        .setTrajectoryId("20081023234104")
                        .setTimestamp(1224805264000L)
                        .setAltitude(226)
                        .setLng(116.306473f)
                        .setLat(40.013867f)
                        .build(),
                PBWaypoint.newBuilder()
                        .setPersonId(1)
                        .setTrajectoryId("20081023234104")
                        .setTimestamp(1224805266000L)
                        .setAltitude(168)
                        .setLng(116.306488f)
                        .setLat(40.013907f)
                        .build()
        );

        processor.run("../data");

        verify(bridge, times(8))
                .send(eq("waypoint-out-0"), waypointCaptor.capture(), eq(MimeType.valueOf("application/x-protobuf")));

        List<PBWaypoint> actual = waypointCaptor.getAllValues();
//        for (PBWaypoint wp: actual) {
//            boolean contains = expected.contains(wp);
//            log.info("contains = {}", contains);
//        }
        assertThat(actual).containsExactlyInAnyOrderElementsOf(expected);
    }
}