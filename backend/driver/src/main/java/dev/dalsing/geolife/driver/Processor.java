package dev.dalsing.geolife.driver;

import dev.dalsing.geolife.pb.PBWaypoint;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import static lombok.AccessLevel.PRIVATE;

@Component
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class Processor {

    StreamBridgeWrapper bridge;
    Counter counter;

    AtomicInteger count = new AtomicInteger(0);

    @Bean
    public static Counter counter(MeterRegistry registry) {
        return registry.counter("driver-count");
    }

    public void run(String... args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("Usage: DriverApp <datadir>, actual: " + Arrays.toString(args));
        }

        String dataDirString = args[0];
        log.info("run: dataDirString = {}", dataDirString);

        File dataDir = new File(dataDirString);
        log.info("run: dataDir = {}", dataDir.getAbsolutePath());

        if (!dataDir.exists() || !dataDir.isDirectory()) {
            throw new IllegalArgumentException("Data directory " + dataDirString + " does not exist for is not a directory");
        }

        String dirName = dataDir.getName();
        if (isInt(dirName)) {
            // data dir points to a specific person ID.  used to demo alert notifs with metered rate and no parallelism.
            log.info("run: processing only for person ID = {}", dirName);
            processTrajectory(new File(dataDir, "Trajectory"), Integer.parseInt(dirName), 500L, 100);
        } else {
            // data dir points to the root of the data tree including all person IDs.  used to bulk import with parallelism.
            log.info("run: processing all person IDs");
            Arrays
                    .stream(Objects.requireNonNull(dataDir.listFiles(File::isDirectory)))
                    .parallel()
                    .forEach(file -> {
                        if (!file.isDirectory()) {
                            throw new IllegalArgumentException("Path " + file.getAbsolutePath() + " is not a directory");
                        }

                        String name = file.getName();
                        int personId = Integer.parseInt(name);

                        File trajectoryDir = new File(file, "Trajectory");

                        processTrajectory(trajectoryDir, personId, 0L, 10000);
                    });

            log.info("run: total count = {}", count.get());
        }

        System.exit(0);
    }

    private void processTrajectory(File trajectoryDir, int personId, long waitTime, int countLogIncrement) {
        File[] pltFiles = trajectoryDir.listFiles(f -> f.getName().toLowerCase().endsWith(".plt"));
        Arrays
                .stream(Objects.requireNonNull(pltFiles))
                .parallel()
                .forEach(pltFile -> {
                    String pltFileName = pltFile.getName();
                    String trajectoryId = pltFileName.substring(0, pltFileName.indexOf('.'));

                    try (FileReader fileReader = new FileReader(pltFile)) {
                        BufferedReader bufferedReader = new BufferedReader(fileReader);

                        // skip past header
                        bufferedReader.readLine();
                        bufferedReader.readLine();
                        bufferedReader.readLine();
                        bufferedReader.readLine();
                        bufferedReader.readLine();
                        bufferedReader.readLine();

                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            String[] fields = line.split(",");

                            float lat = Float.parseFloat(fields[0]);
                            float lng = Float.parseFloat(fields[1]);
                            int alt = Integer.parseInt(fields[2]);
                            String date = fields[5];
                            String time = fields[6];

                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("UTC"));
                            LocalDateTime timestamp = LocalDateTime.parse(date + " " + time, formatter);

                            PBWaypoint pbWaypoint = PBWaypoint
                                    .newBuilder()
                                    .setPersonId(personId)
                                    .setTrajectoryId(trajectoryId)
                                    .setLat(lat)
                                    .setLng(lng)
                                    .setAltitude(alt)
                                    .setTimestamp(timestamp.toInstant(ZoneOffset.UTC).toEpochMilli())
                                    .build();

                            if (waitTime > 0L) {
                                synchronized (this) {
                                    this.wait(250L);
                                }
                            }

                            bridge.send("waypoint-out-0", pbWaypoint, MimeType.valueOf("application/x-protobuf"));

                            counter.increment();
                            if (count.incrementAndGet() % countLogIncrement == 0) {
                                log.info("run: count = {}", count.get());
                            }
                        }
                    } catch (Exception e) {
                        log.error("run: plt file = {}, e = {}", pltFile.getAbsolutePath(), e.toString(), e);
                        throw new IllegalArgumentException(e.toString(), e);
                    }
                });
    }

    private boolean isInt(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
