package dev.dalsing.geolife.driver;

import dev.dalsing.geolife.pb.common.WaypointMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(WaypointMessageConverter.class)
public class DriverApp {

    public static void main(String[] args) {
        SpringApplication.run(DriverApp.class, args);
    }
}
