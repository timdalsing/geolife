package dev.dalsing.geolife.pb.common;

import dev.dalsing.geolife.pb.PBWaypoint;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.converter.AbstractMessageConverter;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.stereotype.Component;
import org.springframework.util.MimeType;

@Component
public class WaypointMessageConverter extends AbstractMessageConverter {

    public WaypointMessageConverter() {
        super(MimeType.valueOf("application/x-protobuf"));
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(PBWaypoint.class);
    }

    @Override
    protected Object convertFromInternal(Message<?> message, Class<?> targetClass, Object conversionHint) {
        try {
            byte[] payload = (byte[]) message.getPayload();
            return PBWaypoint.parseFrom(payload);
        } catch (Exception e) {
            throw new MessageConversionException(e.toString(), e);
        }
    }

    @Override
    protected Object convertToInternal(Object payload, MessageHeaders headers, Object conversionHint) {
        PBWaypoint waypoint = (PBWaypoint) payload;
        return waypoint.toByteArray();
    }
}
