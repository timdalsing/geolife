package dev.dalsing.geolife.pbtojson;

import dev.dalsing.geolife.common.ObjectMapperConfig;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.pb.PBWaypoint;
import dev.dalsing.geolife.pb.common.WaypointMessageConverter;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;
import java.util.function.Function;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@Import({WaypointMessageConverter.class, ObjectMapperConfig.class})
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class PBToJsonService {

    Counter counter;

    public static void main(String[] args) {
        SpringApplication.run(PBToJsonService.class, args);
    }

    @Bean
    public static Counter counter(MeterRegistry registry) {
        return registry.counter("pb-to-json-service-counter");
    }

    @Bean
    public Function<Flux<PBWaypoint>, Flux<GeolifeWaypoint>> processor() {
        return pbWaypointFlux -> pbWaypointFlux
                .doOnEach(s -> counter.increment())
                .map(pbWaypoint -> GeolifeWaypoint
                        .builder()
                        .id(UUID.randomUUID().toString())
                        .personId(pbWaypoint.getPersonId())
                        .trajectoryId(pbWaypoint.getTrajectoryId())
                        .lat(pbWaypoint.getLat())
                        .lng(pbWaypoint.getLng())
                        .altitude(pbWaypoint.getAltitude())
                        .timestamp(convert(pbWaypoint.getTimestamp()))
                        .build());
    }

    private LocalDateTime convert(long ts) {
        Instant instant = Instant.ofEpochMilli(ts);
        return instant.atZone(ZoneOffset.UTC).toLocalDateTime();
    }
}
