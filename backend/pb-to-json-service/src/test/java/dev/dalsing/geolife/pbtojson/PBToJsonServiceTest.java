package dev.dalsing.geolife.pbtojson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.pb.PBWaypoint;
import io.micrometer.core.instrument.Counter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PBToJsonServiceTest {

    PBToJsonService service;

    @Mock
    ObjectMapper mapper;

    @Mock
    Counter counter;

    @BeforeEach
    void setUp() {
        service = new PBToJsonService(counter);
    }

    @Test
    void processor_success() {
        Function<Flux<PBWaypoint>, Flux<GeolifeWaypoint>> function = service.processor();

        int personId = 5;
        String trajectoryId = "some-trajectory-id";
        float lat = 12.34f;
        float lng = 124.34f;
        int alt = 100;
        long ts = System.currentTimeMillis();

        PBWaypoint pbWaypoint = PBWaypoint
                .newBuilder()
                .setPersonId(personId)
                .setTrajectoryId(trajectoryId)
                .setLat(lat)
                .setLng(lng)
                .setAltitude(alt)
                .setTimestamp(ts)
                .build();

        Flux<PBWaypoint> pbWaypointFlux = Flux.just(pbWaypoint);
        Flux<GeolifeWaypoint> geolifeWaypointFlux = function.apply(pbWaypointFlux);
        GeolifeWaypoint geolifeWaypoint = geolifeWaypointFlux.blockFirst();

        assertThat(geolifeWaypoint).isNotNull();

        assertThat(geolifeWaypoint.getPersonId()).isEqualTo(personId);
        assertThat(geolifeWaypoint.getTrajectoryId()).isEqualTo(trajectoryId);
        assertThat(geolifeWaypoint.getLat()).isEqualTo(lat);
        assertThat(geolifeWaypoint.getLng()).isEqualTo(lng);
        assertThat(geolifeWaypoint.getAltitude()).isEqualTo(alt);
        assertThat(geolifeWaypoint.getTimestamp().toInstant(ZoneOffset.UTC).toEpochMilli()).isEqualTo(ts);
    }

    @Test
    void mapper() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        GeolifeWaypoint waypoint = GeolifeWaypoint.builder().timestamp(LocalDateTime.now()).build();
        String s = objectMapper.writeValueAsString(waypoint);
        System.out.println(s);
    }
}