package dev.dalsing.geolife.jdbc.consumer;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.function.Consumer;

import static java.time.temporal.ChronoUnit.MILLIS;

@SpringBootTest
class JDBCConsumerAppIntegrationTest {

    @Autowired
    JDBCConsumerApp app;

    @Autowired
    JdbcTemplate jdbcTemplate;

    static Random random = new Random();

    List<GeolifeWaypoint> waypoints;

    @BeforeEach
    void setUp() {
        jdbcTemplate.update("delete from waypoint");
        waypoints = new ArrayList<>();
        for (int personId = 0; personId < 3; ++personId) {
            for (int trajectoryId = 0; trajectoryId < 4; ++trajectoryId) {
                for (int x = 0; x < 3; ++x) {
                    GeolifeWaypoint waypoint = createWaypoint(personId, "trajectory-" + trajectoryId);
                    waypoints.add(waypoint);
                }
            }
        }
    }

    @AfterEach
    void tearDown() {
        jdbcTemplate.update("delete from waypoint");
    }

    @Tag("integration")
    @Test
    void consumer() {
        Flux<GeolifeWaypoint> waypointFlux = Flux.fromIterable(waypoints);
        Consumer<Flux<GeolifeWaypoint>> consumer = app.consumer();
        consumer.accept(waypointFlux);
        List<GeolifeWaypoint> actual = jdbcTemplate.query("select * from waypoint", (RowMapper<GeolifeWaypoint>) (rs, rowNum) -> GeolifeWaypoint.builder()
                .id(rs.getString(1))
                .personId(rs.getInt(2))
                .trajectoryId(rs.getString(3))
                .lat(rs.getFloat(4))
                .lng(rs.getFloat(5))
                .altitude(rs.getInt(6))
                .timestamp(rs.getTimestamp(7).toLocalDateTime())
                .build());
        Assertions.assertThat(actual).containsAll(waypoints);
    }

    private GeolifeWaypoint createWaypoint(int personId, String trajectoryId) {
        return GeolifeWaypoint.builder()
                .id(UUID.randomUUID().toString())
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(random.nextFloat())
                .lng(random.nextFloat())
                .altitude(random.nextInt())
                .timestamp(LocalDateTime.now().truncatedTo(MILLIS))
                .build();
    }

}