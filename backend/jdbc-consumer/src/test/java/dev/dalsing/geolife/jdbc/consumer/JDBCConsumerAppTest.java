package dev.dalsing.geolife.jdbc.consumer;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import io.micrometer.core.instrument.Timer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Flux;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.function.Consumer;
import java.util.function.IntSupplier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class JDBCConsumerAppTest {

    JDBCConsumerApp app;

    @Mock
    JdbcTemplate jdbcTemplate;

    @Mock
    Timer timer;

    @BeforeEach
    void setUp() {
        app = new JDBCConsumerApp(jdbcTemplate, timer);
    }

    @Test
    void consumer() {
        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 12.34f;
        float lng = 12.34f;
        int alt = 1000;
        LocalDateTime ts = LocalDateTime.now();

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        Flux<GeolifeWaypoint> waypointFlux = Flux.just(waypoint);

        doAnswer(invocation -> {
            IntSupplier supplier = invocation.getArgument(0);
            return supplier.getAsInt();
        }).when(timer).record(any(IntSupplier.class));

        Consumer<Flux<GeolifeWaypoint>> consumer = app.consumer();
        consumer.accept(waypointFlux);

        verify(jdbcTemplate).update("insert into waypoint (id,person_id,trajectory_id,lat,lng,altitude,ts) values (?,?,?,?,?,?,?)",
                id, personId, trajectoryId, lat, lng, alt, Timestamp.valueOf(ts));
    }
}