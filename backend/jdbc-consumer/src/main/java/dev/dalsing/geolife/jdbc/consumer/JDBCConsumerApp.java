package dev.dalsing.geolife.jdbc.consumer;

import dev.dalsing.geolife.common.ObjectMapperConfig;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Flux;

import java.sql.Timestamp;
import java.util.function.Consumer;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@Import(ObjectMapperConfig.class)
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class JDBCConsumerApp {

    JdbcTemplate jdbcTemplate;
    Timer timer;

    public static void main(String[] args) {
        SpringApplication.run(JDBCConsumerApp.class, args);
    }

    @Bean
    public static Timer timer(MeterRegistry registry) {
        return registry.timer("jdbc-consumer-timer");
    }

    @Bean
    public Consumer<Flux<GeolifeWaypoint>> consumer() {
        return flux -> flux.subscribe(waypoint -> {
            timer.record(() ->
                    jdbcTemplate.update("insert into waypoint (id,person_id,trajectory_id,lat,lng,altitude,ts) values (?,?,?,?,?,?,?)",
                            waypoint.getId(),
                            waypoint.getPersonId(),
                            waypoint.getTrajectoryId(),
                            waypoint.getLat(),
                            waypoint.getLng(),
                            waypoint.getAltitude(),
                            Timestamp.valueOf(waypoint.getTimestamp())
                    ));
        });
    }
}
