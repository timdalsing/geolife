package dev.dalsing.geolife.density;

import dev.dalsing.geolife.pb.PBWaypoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.MimeType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static java.time.Duration.ofSeconds;
import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@SpringBootTest
class DensityTest {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    StreamBridge bridge;

    @BeforeEach
    void setUp() {
        jdbcTemplate.update("delete from density_percentage");
        jdbcTemplate.update("delete from density_result");
    }

    @AfterEach
    void tearDown() {
    }

    @Tag("integration")
    @Test
    void density_success() {
        int personId = 1;
        String trajectoryId = UUID.randomUUID().toString();
        float lat = 1.23f;
        float lng = 2.34f;
        int alt = 100;
        LocalDateTime now = LocalDateTime.now().truncatedTo(MILLIS);
        long ts = now.toInstant(UTC).toEpochMilli();

        PBWaypoint pbWaypoint = PBWaypoint.newBuilder()
                .setPersonId(personId)
                .setTrajectoryId(trajectoryId)
                .setLat(lat)
                .setLng(lng)
                .setAltitude(alt)
                .setTimestamp(ts)
                .build();

        bridge.send("pb-out-0", pbWaypoint, MimeType.valueOf("application/x-protobuf"));

        await()
                .pollDelay(ofSeconds(2L))
                .pollInterval(ofSeconds(2L))
                .atMost(ofSeconds(10L))
                .untilAsserted(() -> {
                    List<Integer> result = jdbcTemplate.query("select cluster from density_result", (row, rowNum) -> row.getInt(1));
                    assertThat(result).hasSize(1);
                    assertThat(result.get(0)).isEqualTo(41);

                    List<int[]> alert = jdbcTemplate
                            .query("select cluster, density_count from density_percentage",
                                    (row, rowNum) -> new int[]{row.getInt(1), row.getInt(2)});
                    assertThat(alert).hasSize(1);
                    assertThat(alert.get(0)).isEqualTo(new int[]{41, 1});
                });
    }
}