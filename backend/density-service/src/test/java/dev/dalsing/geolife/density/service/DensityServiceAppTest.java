package dev.dalsing.geolife.density.service;

import dev.dalsing.geolife.model.ClusterStats;
import dev.dalsing.geolife.model.Envelope;
import dev.dalsing.geolife.model.Location;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DensityServiceAppTest {

    DensityServiceApp app;

    @Mock
    JdbcTemplate jdbcTemplate;

    @BeforeEach
    void setUp() {
        app = new DensityServiceApp(jdbcTemplate);
    }

    @Test
    void getClusters() {
        List<long[]> counts = List.of(new long[]{1L,2L});

        int clusterId = 2;
        Location centroid = Location.builder().lat(12.34f).lng(23.45f).build();

        Envelope envelope = Envelope.builder()
                .lowerLeft(Location.builder().lng(21.43f).lat(54.32f).build())
                .upperRight(Location.builder().lng(12.34f).lat(23.45f).build())
                .build();

        double percentage = 0.01;

        ClusterStats clusterStats = ClusterStats.builder()
                .cluster(clusterId)
                .centroid(centroid)
                .envelope(envelope)
                .percentage(percentage)
                .build();

        List<ClusterStats> expected = List.of(clusterStats);

        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(counts).thenReturn(expected);

        Mono<List<ClusterStats>> clusters = app.getClusterStats();
        StepVerifier.create(clusters).assertNext(actual -> assertThat(actual).isEqualTo(expected)).verifyComplete();
    }
}