package dev.dalsing.geolife.density.service;

import dev.dalsing.geolife.model.ClusterStats;
import dev.dalsing.geolife.model.Envelope;
import dev.dalsing.geolife.model.Location;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SpringBootTest
class DensityServiceAppIntegrationTest {

    @Autowired
    DensityServiceApp app;

    @Autowired
    JdbcTemplate jdbcTemplate;

    static Random random = new Random();

    static float minLng = -81.511399f;
    static float maxLng = -81.462928f;
    static float minLat = 30.064383f;
    static float maxLat = 30.090180f;

    Collection<ClusterStats> expected;

    @BeforeEach
    void setUp() {
        // cleanup
        jdbcTemplate.update("delete from density_result");
        jdbcTemplate.update("delete from waypoint");

        // temp holders
        Map<Integer, Long> counts = new HashMap<>();
        Map<Integer, Envelope> envelopeMap = new HashMap<>();

        // generate clusters
        Map<Integer, ClusterStats> statsMap = IntStream
                .range(0, 10)
                .mapToObj(c -> {
                    float lat = minLat + (random.nextFloat() * (maxLat - minLat));
                    float lng = minLng + (random.nextFloat() * (maxLng - minLng));

                    return ClusterStats.builder()
                            .cluster(c)
                            .centroid(Location.builder().lat(lat).lng(lng).build())
                            .build();
                })
                .collect(Collectors.toMap(ClusterStats::getCluster, t -> t));

        // generate waypoints and corresponding density results
        for (int w = 0; w < 1000; ++w) {
            int c = random.nextInt(10);// random cluster

            // add or update count for cluster
            Long count = counts.computeIfAbsent(c, t -> 0L);
            counts.put(c, count + 1L);

            // corresponding cluster
            ClusterStats clusterStats = statsMap.get(c);

            // random values for lat/lng within same box as centroid for cluster
            float lat = minLat + (random.nextFloat() * (maxLat - minLat));
            float lng = minLng + (random.nextFloat() * (maxLng - minLng));

            // update envelope
            Envelope envelope = envelopeMap.computeIfAbsent(c, t -> Envelope.builder()
                    .lowerLeft(Location.builder().lat(Float.MAX_VALUE).lng(Float.MAX_VALUE).build())
                    .upperRight(Location.builder().lat(-Float.MAX_VALUE).lng(-Float.MAX_VALUE).build())
                    .build());

            float minLat = Math.min(lat, envelope.getLowerLeft().getLat());
            float minLng = Math.min(lng, envelope.getLowerLeft().getLng());
            float maxLat = Math.max(lat, envelope.getUpperRight().getLat());
            float maxLng = Math.max(lng, envelope.getUpperRight().getLng());

            envelope.getLowerLeft().setLat(minLat);
            envelope.getLowerLeft().setLng(minLng);
            envelope.getUpperRight().setLat(maxLat);
            envelope.getUpperRight().setLng(maxLng);

            // insert rows
            String waypointId = "waypoint-" + w;
            String trajectoryId = UUID.randomUUID().toString();
            int personId = random.nextInt(100);

            jdbcTemplate.update("insert into waypoint (id,person_id,trajectory_id,lat,lng,altitude,ts) values (?,?,?,?,?,?,?)",
                    waypointId, personId, trajectoryId, lat, lng, 1, LocalDateTime.now());

            jdbcTemplate.update("insert into density_result (waypoint_id, cluster, centroid_lng, centroid_lat) values (?,?,?,?)",
                    waypointId, c, clusterStats.getCentroid().getLng(), clusterStats.getCentroid().getLat());
        }

        // calculate sum for percentages
        long sum = counts.values().stream().mapToLong(t -> t).sum();

        // calculate percent, add envelope to stats
        for (Map.Entry<Integer, ClusterStats> e : statsMap.entrySet()) {
            int c = e.getKey();
            ClusterStats stats = e.getValue();

            Envelope envelope = envelopeMap.get(c);
            stats.setEnvelope(envelope);

            long count = counts.get(c);
            double percent = 1.0 * count / sum;
            stats.setPercentage(percent);
        }

        expected = new ArrayList<>(statsMap.values());
    }

    @Tag("integration")
    @Test
    void getClusters() {
        Mono<List<ClusterStats>> clusters = app.getClusterStats();
        StepVerifier.create(clusters).assertNext(actual -> Assertions.assertThat(actual).isEqualTo(expected)).verifyComplete();
    }
}