package dev.dalsing.geolife.density.service;

import dev.dalsing.geolife.common.ObjectMapperConfig;
import dev.dalsing.geolife.model.ClusterStats;
import dev.dalsing.geolife.model.Envelope;
import dev.dalsing.geolife.model.Location;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@RestController
@Import({ObjectMapperConfig.class})
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class DensityServiceApp {

    JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(DensityServiceApp.class, args);
    }

    @GetMapping("/cluster")
    public Mono<List<ClusterStats>> getClusterStats() {
        // get counts
        Map<Integer, Long> counts = jdbcTemplate.query("select count(d.cluster) c, d.cluster from density_result d group by d.cluster order by c desc",
                        (row, rowNum) -> new long[]{row.getInt(1), row.getLong(2)})
                .stream()
                .collect(Collectors.toMap(t -> (int) t[1], t -> t[0]));

        // calculate total
        long sum = counts.values().stream().mapToLong(t -> t).sum();

        // get envelope for each cluster
        Map<Integer, ClusterStats> statsMap = jdbcTemplate
                .query("select d.cluster, min(d.centroid_lng), min(d.centroid_lat), min(w.lng) minLng, min(w.lat) minLat, max(w.lng) maxLng, max(w.lat) maxLat from density_result d inner join waypoint w on w.id = d.waypoint_id group by d.cluster order by d.cluster",
                        (row, rowNum) -> ClusterStats.builder()
                                .cluster(row.getInt(1))
                                .centroid(Location.builder().lng(row.getFloat(2)).lat(row.getFloat(3)).build())
                                .envelope(Envelope.builder()
                                        .lowerLeft(Location.builder().lng(row.getFloat(4)).lat(row.getFloat(5)).build())
                                        .upperRight(Location.builder().lng(row.getFloat(6)).lat(row.getFloat(7)).build())
                                        .build())
                                .build()).stream().collect(Collectors.toMap(ClusterStats::getCluster, t -> t));

        // add percentages
        for (Map.Entry<Integer, ClusterStats> e : statsMap.entrySet()) {
            int cluster = e.getKey();
            Long count = counts.get(cluster);
            double percent = 1.0 * count / sum;
            e.getValue().setPercentage(percent);
        }

        return Mono.just(new ArrayList<>(statsMap.values()));
    }
}
