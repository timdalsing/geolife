package dev.dalsing.geolife.mongo.service;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.mongo.common.MongoWaypoint;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.ReactiveFindOperation.ReactiveFind;
import org.springframework.data.mongodb.core.ReactiveFindOperation.TerminatingFind;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@ExtendWith(MockitoExtension.class)
@Slf4j
class MongoServiceAppTest {

    MongoServiceApp app;

    @Mock
    ReactiveMongoTemplate template;

    static Random random = new Random();

    String id1 = UUID.randomUUID().toString();
    String id2 = UUID.randomUUID().toString();
    int personId = 3;
    String trajectoryId = "some-trajectory-id";
    float lat = random.nextFloat();
    float lng = random.nextFloat();
    int alt = random.nextInt(1000);
    LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

    GeolifeWaypoint waypoint1 = GeolifeWaypoint.builder()
            .id(id1)
            .personId(personId)
            .trajectoryId(trajectoryId)
            .lat(lat)
            .lng(lng)
            .altitude(alt)
            .timestamp(ts)
            .build();

    GeolifeWaypoint waypoint2 = GeolifeWaypoint.builder()
            .id(id2)
            .personId(personId)
            .trajectoryId(trajectoryId)
            .lat(lat)
            .lng(lng)
            .altitude(alt)
            .timestamp(ts)
            .build();
    MongoWaypoint mongoWaypoint1 = MongoWaypoint.builder()
            .id(id1)
            .personId(personId)
            .trajectoryId(trajectoryId)
            .lat(lat)
            .lng(lng)
            .altitude(alt)
            .timestamp(ts)
            .build();

    MongoWaypoint mongoWaypoint2 = MongoWaypoint.builder()
            .id(id2)
            .personId(personId)
            .trajectoryId(trajectoryId)
            .lat(lat)
            .lng(lng)
            .altitude(alt)
            .timestamp(ts)
            .build();

    @BeforeEach
    void setUp() {
        app = new MongoServiceApp(template);
    }

    @Test
    void findTrajectoryIdsForPerson_success() {
        ReactiveFind<MongoWaypoint> reactiveFind = mock(ReactiveFind.class);
        TerminatingFind<MongoWaypoint> terminatingFind = mock(TerminatingFind.class);

        when(template.query(eq(MongoWaypoint.class))).thenReturn(reactiveFind);
        when(reactiveFind.matching(any(Query.class))).thenReturn(terminatingFind);

        Flux<MongoWaypoint> waypointFlux = Flux.just(mongoWaypoint1, mongoWaypoint2);
        when(terminatingFind.all()).thenReturn(waypointFlux);

        Mono<List<String>> trajectoryIdsForPerson = app.findTrajectoryIdsForPerson(personId);
        StepVerifier.create(trajectoryIdsForPerson).assertNext(list -> Assertions.assertThat(list).contains(trajectoryId)).verifyComplete();
    }

    @Test
    void findWaypointsForPerson_success() {
        ReactiveFind<MongoWaypoint> reactiveFind = mock(ReactiveFind.class);
        TerminatingFind<MongoWaypoint> terminatingFind = mock(TerminatingFind.class);

        when(template.query(eq(MongoWaypoint.class))).thenReturn(reactiveFind);
        when(reactiveFind.matching(any(Query.class))).thenReturn(terminatingFind);

        Flux<MongoWaypoint> waypointFlux = Flux.just(mongoWaypoint1, mongoWaypoint2);
        when(terminatingFind.all()).thenReturn(waypointFlux);

        Mono<List<GeolifeWaypoint>> waypointsForPerson = app.findWaypointsForPerson(personId);
        StepVerifier.create(waypointsForPerson).assertNext(list -> Assertions.assertThat(list).isEqualTo(List.of(waypoint1, waypoint2))).verifyComplete();

        verify(reactiveFind).matching(query(where("personId").is(personId)));
    }

    @Disabled("disable until we can figure out problem with mocking")
    @Test
    void findWaypointsForPersonAndTrajectory_success() {
        ReactiveFind<MongoWaypoint> reactiveFind = mock(ReactiveFind.class);
        TerminatingFind<MongoWaypoint> terminatingFind = mock(TerminatingFind.class);

        when(template.query(eq(MongoWaypoint.class))).thenReturn(reactiveFind);
        when(reactiveFind.matching(any(Query.class))).thenReturn(terminatingFind);

        Flux<MongoWaypoint> waypointFlux = Flux.just(mongoWaypoint1, mongoWaypoint2);
        when(terminatingFind.all()).thenReturn(waypointFlux);
        when(reactiveFind.all()).thenReturn(waypointFlux);

        Mono<List<GeolifeWaypoint>> actualMono = app.findWaypointsForPersonAndTrajectory(0, trajectoryId, 0);

        GeolifeWaypoint[] expectedWaypoints = Arrays.stream(new GeolifeWaypoint[]{waypoint1, waypoint2})
                .filter(wp -> wp.getPersonId() == 0 && wp.getTrajectoryId().equals(trajectoryId))
                .toArray(GeolifeWaypoint[]::new);
        StepVerifier.create(actualMono).assertNext(actual -> assertThat(actual).containsExactlyInAnyOrder(expectedWaypoints)).verifyComplete();

        verify(reactiveFind).matching(query(where("personId").is(personId).and("trajectoryId").is(trajectoryId)));
    }
}