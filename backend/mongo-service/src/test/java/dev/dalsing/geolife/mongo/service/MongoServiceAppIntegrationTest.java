package dev.dalsing.geolife.mongo.service;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.mongo.common.MongoWaypoint;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.test.context.ActiveProfiles;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("server")
@Slf4j
class MongoServiceAppIntegrationTest {

    @Autowired
    MongoServiceApp app;

    @Autowired
    ReactiveMongoTemplate template;

    static Random random = new Random();

    List<GeolifeWaypoint> waypoints;
    List<MongoWaypoint> mongoWaypoints;

    @BeforeEach
    void setUp() {
        template.remove(MongoWaypoint.class).all().subscribe();
        waypoints = new ArrayList<>();
        mongoWaypoints = new ArrayList<>();
        for (int personId = 0; personId < 3; ++personId) {
            for (int trajectoryId = 0; trajectoryId < 4; ++trajectoryId) {
                for (int x = 0; x < 3; ++x) {
                    GeolifeWaypoint waypoint = createWaypoint(personId, "trajectory-" + trajectoryId);
                    waypoints.add(waypoint);
                    mongoWaypoints.add(createMongoWaypoint(waypoint));
                }
            }
        }
        template.insertAll(mongoWaypoints).blockLast();
    }

    @AfterEach
    void tearDown() {
        template.remove(MongoWaypoint.class).all().block();
    }

    @Tag("integration")
    @Test
    void getPersonIds_success() {
        Mono<List<Integer>> personIds = app.getPersonIds();
        StepVerifier.create(personIds).assertNext(list -> assertThat(list).containsExactlyInAnyOrder(0,1,2,3)).expectComplete();
    }

    @Tag("integration")
    @Test
    void findTrajectoryIdsForPerson_success() {
        Mono<List<String>> trajectoryIdsForPerson = app.findTrajectoryIdsForPerson(0);
        StepVerifier.create(trajectoryIdsForPerson).assertNext(list -> assertThat(list)
                .containsExactly("trajectory-0", "trajectory-1", "trajectory-2", "trajectory-3")).verifyComplete();
    }

    @Tag("integration")
    @Test
    void findWaypointsForPerson_success() {
        Mono<List<GeolifeWaypoint>> waypointsForPerson = app.findWaypointsForPerson(0);
        List<GeolifeWaypoint> expected = waypoints.stream().filter(wp -> wp.getPersonId() == 0).collect(Collectors.toList());
        StepVerifier.create(waypointsForPerson).assertNext(list -> assertThat(list).containsAll(expected)).verifyComplete();
    }

    @Tag("integration")
    @Test
    void findWaypointsForPersonAndTrajectory_success() {
        String trajectoryId = "trajectory-1";
        Mono<List<GeolifeWaypoint>> actualMono = app.findWaypointsForPersonAndTrajectory(0, trajectoryId, 0);

        GeolifeWaypoint[] expectedWaypoints = waypoints
                .stream()
                .filter(wp -> wp.getPersonId() == 0 && wp.getTrajectoryId().equals(trajectoryId))
                .toArray(GeolifeWaypoint[]::new);
        StepVerifier.create(actualMono).assertNext(actual -> assertThat(actual).containsExactlyInAnyOrder(expectedWaypoints)).verifyComplete();
    }

    private GeolifeWaypoint createWaypoint(int personId, String trajectoryId) {
        return GeolifeWaypoint.builder()
                .id(UUID.randomUUID().toString())
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(random.nextFloat())
                .lng(random.nextFloat())
                .altitude(random.nextInt())
                .timestamp(LocalDateTime.now().truncatedTo(MILLIS))
                .build();
    }
    private MongoWaypoint createMongoWaypoint(GeolifeWaypoint wp) {
        return MongoWaypoint.builder()
                .id(wp.getId())
                .personId(wp.getPersonId())
                .trajectoryId(wp.getTrajectoryId())
                .lat(wp.getLat())
                .lng(wp.getLng())
                .altitude(wp.getAltitude())
                .timestamp(wp.getTimestamp())
                .build();
    }
}