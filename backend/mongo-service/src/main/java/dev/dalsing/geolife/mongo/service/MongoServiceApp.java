package dev.dalsing.geolife.mongo.service;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.mongo.common.MongoWaypoint;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Limit;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@SpringBootApplication
@RestController
@CrossOrigin
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class MongoServiceApp {

    ReactiveMongoTemplate template;

    public static void main(String[] args) {
        SpringApplication.run(MongoServiceApp.class, args);
    }

    @GetMapping("/personIds")
    public Mono<List<Integer>> getPersonIds() {
        return template
                .query(MongoWaypoint.class)
                .distinct("personId")
                .all()
                .map(o -> (Integer) o)
                .sort()
                .collectList();
    }

    @GetMapping("/trajectory/{personId}")
    public Mono<List<String>> findTrajectoryIdsForPerson(@PathVariable("personId") int personId) {
        return template
                .query(MongoWaypoint.class)
                .matching(query(where("personId").is(personId)))
                .all()
                .map(MongoWaypoint::getTrajectoryId)
                .distinct()
                .sort()
                .collectList();
    }

    @GetMapping("/waypoint/{personId}")
    public Mono<List<GeolifeWaypoint>> findWaypointsForPerson(@PathVariable("personId") int personId) {
        return template
                .query(MongoWaypoint.class)
                .matching(query(where("personId").is(personId)))
                .all()
                .map(mwp -> GeolifeWaypoint.builder()
                        .id(mwp.getId())
                        .personId(mwp.getPersonId())
                        .trajectoryId(mwp.getTrajectoryId())
                        .lat(mwp.getLat())
                        .lng(mwp.getLng())
                        .altitude(mwp.getAltitude())
                        .timestamp(mwp.getTimestamp())
                        .build())
                .collectList();
    }

    @GetMapping("/waypoint/{personId}/{trajectoryId}")
    public Mono<List<GeolifeWaypoint>> findWaypointsForPersonAndTrajectory(
            @PathVariable("personId") int personId,
            @PathVariable("trajectoryId") String trajectoryId,
            @RequestParam(name = "limit", required = false, defaultValue = "100") int limit) {

        Limit l = limit == 0 ? Limit.unlimited() : Limit.of(limit);

        return template
                .query(MongoWaypoint.class)
                .matching(query(where("personId").is(personId).and("trajectoryId").is(trajectoryId)).limit(l))
                .all()
                .map(mwp -> GeolifeWaypoint.builder()
                        .id(mwp.getId())
                        .personId(mwp.getPersonId())
                        .trajectoryId(mwp.getTrajectoryId())
                        .lat(mwp.getLat())
                        .lng(mwp.getLng())
                        .altitude(mwp.getAltitude())
                        .timestamp(mwp.getTimestamp())
                        .build())
                .collectList();
    }
}
