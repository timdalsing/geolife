package dev.dalsing.geolife.mongo.common;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@Document("waypoint")
public class MongoWaypoint {

    @Id
    String id;
    @Indexed
    int personId;
    @Indexed
    String trajectoryId;
    float lat;
    float lng;
    int altitude;
    LocalDateTime timestamp;
}
