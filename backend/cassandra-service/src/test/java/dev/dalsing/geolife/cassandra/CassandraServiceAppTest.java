package dev.dalsing.geolife.cassandra;

import dev.dalsing.cassandra.common.*;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.cassandra.core.CassandraAdminTemplate;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CassandraServiceAppTest {

    CassandraServiceApp app;

    @Mock
    WaypointRepo repo;

    @Mock
    ReactiveCassandraTemplate template;

    @Mock
    CassandraAdminTemplate adminTemplate;

    static Random random = new Random();
    LocalDateTime now = LocalDateTime.now().truncatedTo(MILLIS);

    @BeforeEach
    void setUp() {
        app = new CassandraServiceApp(repo, template, adminTemplate);
    }

    @Test
    void findTrajectoryIdsForPerson_success() {
        int personId = 3;
        String trajectoryId1 = "trajectory-1";
        String trajectoryId2 = "trajectory-2";

        Flux<Object> trajectoryIdFlux = Flux.just(trajectoryId1, trajectoryId2);
        when(template.select(anyString(), any())).thenReturn(trajectoryIdFlux);
        Mono<List<String>> trajectoryIdsForPerson = app.findTrajectoryIdsForPerson(personId, 0);
        StepVerifier.create(trajectoryIdsForPerson).assertNext(list -> Assertions.assertThat(list).contains(trajectoryId1, trajectoryId2)).verifyComplete();
    }

    @Test
    void findWaypointsForPerson_success() {
        int personId = 3;
        String trajectoryId = "some-trajectory-id";
        GeolifeWaypoint waypoint = createWaypoint(personId, trajectoryId);
        CassandraWaypoint cassandraWaypoint = createCassandraWaypoint(waypoint);

        when(repo.findByKeyPersonId(anyInt(), any())).thenReturn(Flux.just(cassandraWaypoint));

        Mono<List<GeolifeWaypoint>> waypointMono = app.findWaypointsForPerson(personId, 0);
        List<GeolifeWaypoint> waypointList = List.of(waypoint);
        StepVerifier.create(waypointMono).expectNext(waypointList).verifyComplete();
    }

    @Test
    void findWaypointsForPersonAndTrajectory_success() {
        int personId = 3;
        String trajectoryId = "some-trajectory-id";
        GeolifeWaypoint waypoint = createWaypoint(personId, trajectoryId);
        CassandraWaypoint cassandraWaypoint = createCassandraWaypoint(waypoint);

        when(repo.findByKeyPersonIdAndKeyTrajectoryId(anyInt(), anyString(), any())).thenReturn(Flux.just(cassandraWaypoint));

        Flux<GeolifeWaypoint> actualFlux = app.findWaypointsForPersonAndTrajectory(personId, trajectoryId, 0);

        StepVerifier
                .create(actualFlux)
                .assertNext(actual -> {
                    assertThat(actual.getId()).isEqualTo(waypoint.getId());
                    assertThat(actual.getPersonId()).isEqualTo(waypoint.getPersonId());
                })
                .verifyComplete();
    }

    private GeolifeWaypoint createWaypoint(int personId, String trajectoryId) {
        return GeolifeWaypoint.builder()
                .id(UUID.randomUUID().toString())
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(random.nextFloat())
                .lng(random.nextFloat())
                .altitude(random.nextInt(1000))
                .timestamp(now)
                .build();
    }

    private CassandraWaypoint createCassandraWaypoint(GeolifeWaypoint waypoint) {
        return CassandraWaypoint.builder()
                .key(CassandraWaypointKey.builder()
                        .id(waypoint.getId())
                        .personId(waypoint.getPersonId())
                        .trajectoryId(waypoint.getTrajectoryId())
                        .build())
                .lat(waypoint.getLat())
                .lng(waypoint.getLng())
                .altitude(waypoint.getAltitude())
                .timestamp(waypoint.getTimestamp())
                .build();
    }
}