package dev.dalsing.geolife.cassandra;

import dev.dalsing.cassandra.common.*;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("server")
class CassandraServiceAppIntegrationTest {

    @Autowired
    CassandraServiceApp app;

    @Autowired
    WaypointRepo repo;

    @Autowired
    PersonTrajectoryRepo personTrajectoryRepo;

    static Random random = new Random();

    String id1 = UUID.randomUUID().toString();
    String id2 = UUID.randomUUID().toString();
    int personId = random.nextInt(1000);
    String trajectoryId = UUID.randomUUID().toString();
    float lat = random.nextFloat();
    float lng = random.nextFloat();
    int alt = random.nextInt(1000);
    LocalDateTime ts = LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS);

    @BeforeEach
    void setUp() {
        CassandraWaypoint cassandraWaypoint1 = CassandraWaypoint.builder()
                .key(CassandraWaypointKey.builder()
                        .id(id1)
                        .personId(personId)
                        .trajectoryId(trajectoryId)
                        .build())
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();
        CassandraWaypoint cassandraWaypoint2 = CassandraWaypoint.builder()
                .key(CassandraWaypointKey.builder()
                        .id(id2)
                        .personId(personId)
                        .trajectoryId(trajectoryId)
                        .build())
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        CassandraPersonTrajectory personTrajectory = CassandraPersonTrajectory.builder()
                .key(CassandraPersonTrajectoryKey.builder()
                        .personId(personId)
                        .trajectoryId(trajectoryId)
                        .build())
                .build();

        repo.saveAll(List.of(cassandraWaypoint1, cassandraWaypoint2)).collectList().block();
        personTrajectoryRepo.save(personTrajectory).block();
    }

    @AfterEach
    void tearDown() {
        repo.deleteById(CassandraWaypointKey.builder().id(id1).personId(personId).trajectoryId(trajectoryId).build()).block();
        repo.deleteById(CassandraWaypointKey.builder().id(id2).personId(personId).trajectoryId(trajectoryId).build()).block();
        personTrajectoryRepo.deleteById(CassandraPersonTrajectoryKey.builder().personId(personId).trajectoryId(trajectoryId).build()).block();
    }

    @Test
    void getPersonIds_success() {
        Mono<List<Integer>> personIds = app.getPersonIds();
        StepVerifier.create(personIds).assertNext(list -> assertThat(list).containsExactlyInAnyOrder(0, 1, 2, 3)).expectComplete();
    }

    @Tag("integration")
    @Test
    void findTrajectoryIdsForPerson_success() {
        Mono<List<String>> trajectoryIdsForPerson = app.findTrajectoryIdsForPerson(personId, 0);
        StepVerifier.create(trajectoryIdsForPerson).assertNext(list -> assertThat(list).contains(trajectoryId)).verifyComplete();
    }

    @Tag("integration")
    @Test
    void findWaypointsForPerson_success() {
        GeolifeWaypoint waypoint1 = GeolifeWaypoint.builder()
                .id(id1)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();
        GeolifeWaypoint waypoint2 = GeolifeWaypoint.builder()
                .id(id2)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        Mono<List<GeolifeWaypoint>> waypointsForPerson = app.findWaypointsForPerson(personId, 0);

        List<GeolifeWaypoint> waypointList = List.of(waypoint1, waypoint2);

        StepVerifier.create(waypointsForPerson).assertNext(list -> assertThat(waypointList).containsAll(list)).verifyComplete();
    }

    @Tag("integration")
    @Test
    void findWaypointsForPersonAndTrajectory_success() {
        GeolifeWaypoint waypoint1 = GeolifeWaypoint.builder()
                .id(id1)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();
        GeolifeWaypoint waypoint2 = GeolifeWaypoint.builder()
                .id(id2)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        Mono<List<GeolifeWaypoint>> listMono = app.findWaypointsForPersonAndTrajectory(personId, trajectoryId, 0).collectList();

        StepVerifier
                .create(listMono)
                .assertNext(actual -> Assertions.assertThat(actual).containsExactlyInAnyOrder(waypoint1, waypoint2))
                .verifyComplete();
    }
}