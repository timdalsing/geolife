package dev.dalsing.geolife.cassandra;

import com.datastax.oss.driver.api.core.CqlIdentifier;
import com.datastax.oss.driver.api.core.CqlSession;
import dev.dalsing.cassandra.common.CassandraPersonTrajectoryKey;
import dev.dalsing.cassandra.common.CassandraWaypoint;
import dev.dalsing.cassandra.common.WaypointRepo;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.cassandra.core.CassandraAdminTemplate;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import org.springframework.data.cassandra.repository.config.EnableReactiveCassandraRepositories;
import org.springframework.data.domain.Limit;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@RestController
@CrossOrigin
@EnableReactiveCassandraRepositories(basePackages = "dev.dalsing.cassandra.common")
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class CassandraServiceApp {

    WaypointRepo repo;
    ReactiveCassandraTemplate template;
    CassandraAdminTemplate adminTemplate;

    public static void main(String[] args) {
        SpringApplication.run(CassandraServiceApp.class, args);
    }

    @Bean
    public static CassandraAdminTemplate adminTemplate(CqlSession session) {
        return new CassandraAdminTemplate(session);
    }

    @GetMapping("/personIds")
    public Mono<List<Integer>> getPersonIds() {
        return template
                .select("select distinct \"personId\" from person_trajectory", Integer.class)
                .sort()
                .collectList();
    }

    @GetMapping("/trajectory/{personId}")
    public Mono<List<String>> findTrajectoryIdsForPerson(
            @PathVariable("personId") int personId,
            @RequestParam(name = "limit", required = false, defaultValue = "100") int limit) {

        String cql = limit == 0
                ? String.format("select \"trajectoryId\" from person_trajectory where \"personId\" = %d", personId)
                : String.format("select \"trajectoryId\" from person_trajectory where \"personId\" = %d limit %d", personId, limit);

        return template.select(cql, String.class).collectList();
    }

    @GetMapping("/waypoint/{personId}")
    public Mono<List<GeolifeWaypoint>> findWaypointsForPerson(
            @PathVariable("personId") int personId,
            @RequestParam(name = "limit", required = false, defaultValue = "100") int limit) {

        Limit l = limit == 0 ? Limit.unlimited() : Limit.of(limit);

        return repo
                .findByKeyPersonId(personId, l)
                .map(cassandraWaypoint -> GeolifeWaypoint.builder()
                        .id(cassandraWaypoint.getKey().getId())
                        .personId(cassandraWaypoint.getKey().getPersonId())
                        .trajectoryId(cassandraWaypoint.getKey().getTrajectoryId())
                        .lat(cassandraWaypoint.getLat())
                        .lng(cassandraWaypoint.getLng())
                        .altitude(cassandraWaypoint.getAltitude())
                        .timestamp(cassandraWaypoint.getTimestamp())
                        .build())
                .distinct(GeolifeWaypoint::getTrajectoryId)
                .collectList();
    }

    @GetMapping("/waypoint/{personId}/{trajectoryId}")
    public Flux<GeolifeWaypoint> findWaypointsForPersonAndTrajectory(
            @PathVariable("personId") int personId,
            @PathVariable("trajectoryId") String trajectoryId,
            @RequestParam(name = "limit", required = false, defaultValue = "100") int limit) {

        Limit l = limit == 0 ? Limit.unlimited() : Limit.of(limit);

        return repo
                .findByKeyPersonIdAndKeyTrajectoryId(personId, trajectoryId, l)
                .map(cassandraWaypoint -> GeolifeWaypoint.builder()
                        .id(cassandraWaypoint.getKey().getId())
                        .personId(cassandraWaypoint.getKey().getPersonId())
                        .trajectoryId(cassandraWaypoint.getKey().getTrajectoryId())
                        .lat(cassandraWaypoint.getLat())
                        .lng(cassandraWaypoint.getLng())
                        .altitude(cassandraWaypoint.getAltitude())
                        .timestamp(cassandraWaypoint.getTimestamp())
                        .build());
    }

    @PostConstruct
    public void post() {
        adminTemplate.createTable(true, CqlIdentifier.fromCql("waypoint"), CassandraWaypoint.class, new HashMap<>());
        adminTemplate.createTable(true, CqlIdentifier.fromCql("person_trajectory"), CassandraPersonTrajectoryKey.class, new HashMap<>());
    }
}
