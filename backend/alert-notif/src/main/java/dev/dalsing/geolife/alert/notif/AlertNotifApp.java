package dev.dalsing.geolife.alert.notif;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlertNotifApp {

    public static void main(String[] args) {
        SpringApplication.run(AlertNotifApp.class, args);
    }
}
