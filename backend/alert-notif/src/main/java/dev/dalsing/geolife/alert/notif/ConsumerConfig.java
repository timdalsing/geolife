package dev.dalsing.geolife.alert.notif;

import dev.dalsing.geolife.model.DensityAlert;
import dev.dalsing.geolife.model.DensityResult;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.function.Consumer;

import static lombok.AccessLevel.PRIVATE;

@Configuration
@RestController
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ConsumerConfig {

    SimpMessagingTemplate template;

    @Bean
    public Consumer<Flux<Message<DensityAlert>>> consumer() {
        return flux -> flux.subscribe(message -> {
            DensityAlert alert = message.getPayload();
            log.info("consumer: alert = {}", alert);
            DensityResult result = alert.getResult();
            GeolifeWaypoint waypoint = result.getWaypoint();

            int personId = waypoint.getPersonId();
            String destination = String.format("/queue/%d", personId);
            log.info("consumer: personId = {}, destination = {}", personId, destination);

            template.convertAndSend(destination, alert);
        });
    }
}
