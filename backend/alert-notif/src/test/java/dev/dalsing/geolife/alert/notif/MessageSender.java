package dev.dalsing.geolife.alert.notif;

import dev.dalsing.geolife.model.DensityAlert;
import dev.dalsing.geolife.model.DensityResult;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.http.MediaType;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.time.LocalDateTime;
import java.util.stream.IntStream;

import static java.time.temporal.ChronoUnit.MILLIS;

@SpringBootTest(classes = MessageSender.TestApp.class)
public class MessageSender {

    @Autowired
    StreamBridge bridge;

    @Test
    void sendMessage() {
        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        int cluster = 3;
        double[] centroid = {12.3, 23.4};
        DensityResult result = DensityResult.builder()
                .waypoint(waypoint)
                .cluster(cluster)
                .centroid(centroid)
                .build();

        long count = 2;
        double percentage = 0.12;
        DensityAlert alert = DensityAlert.builder()
                .result(result)
                .count(count)
                .percentage(percentage)
                .build();

        Message<DensityAlert> message = MessageBuilder.withPayload(alert).build();

        IntStream.range(0, 10).forEach(t -> {
            GeolifeWaypoint wp = message.getPayload().getResult().getWaypoint();
            wp.setLat((float) (wp.getLat() + t * 0.001));
            wp.setLng((float) (wp.getLng() + t * 0.001));
            bridge.send("density-alert", message, MediaType.APPLICATION_JSON);
        });
    }

    @SpringBootApplication
    public static class TestApp {
        public static void main(String[] args) {
            SpringApplication.run(TestApp.class, args);
        }
    }
}
