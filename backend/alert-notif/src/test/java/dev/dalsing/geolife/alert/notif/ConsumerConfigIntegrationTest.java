package dev.dalsing.geolife.alert.notif;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import dev.dalsing.geolife.model.DensityAlert;
import dev.dalsing.geolife.model.DensityResult;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.Message;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import reactor.core.publisher.Flux;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicReference;

import static java.time.Duration.ofSeconds;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.awaitility.Awaitility.await;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.DEFINED_PORT;

@SpringBootTest(webEnvironment = DEFINED_PORT)
@Slf4j
class ConsumerConfigIntegrationTest {

    @Autowired
    ConsumerConfig consumerConfig;

    AtomicReference<DensityAlert> actual = new AtomicReference<>(null);

    final Object sync = new Object();

    @BeforeEach
    void setUp() {
    }

    @Tag("integration")
    @Test
    void consumer() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter();
        messageConverter.setObjectMapper(objectMapper);

        WebSocketClient webSocketClient = new StandardWebSocketClient();
//        SockJsClient sockJsClient = new SockJsClient(List.of(new WebSocketTransport(webSocketClient)));
        WebSocketStompClient webSocketStompClient = new WebSocketStompClient(webSocketClient);
        webSocketStompClient.setMessageConverter(messageConverter);

        webSocketStompClient.connect("ws://localhost:8631/notif", new StompSessionHandler() {
            @Override
            public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
                log.info("afterConnected: session = {}, headers = {}", session.getSessionId(), connectedHeaders);
                session.subscribe("/queue/0", this);
                synchronized (sync) {
                    sync.notify();
                }
            }

            @Override
            public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
                log.error("handleException: headers = {}, exception = {}", headers, exception, exception);
            }

            @Override
            public void handleTransportError(StompSession session, Throwable exception) {
                log.error("handleTransportError: exception = {}", exception, exception);
            }

            @Override
            public Type getPayloadType(StompHeaders headers) {
                log.info("getPayloadType: headers = {}", headers);
                return DensityAlert.class;
            }

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                log.info("handleFrame: payload = {}", payload);
                DensityAlert alert = (DensityAlert) payload;
                actual.set(alert);
            }
        });

        synchronized (sync) {
            sync.wait(5000L);
        }

        String id = "some-id";
        int personId = 0;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        int cluster = 3;
        double[] centroid = {12.3, 23.4};
        DensityResult result = DensityResult.builder()
                .waypoint(waypoint)
                .cluster(cluster)
                .centroid(centroid)
                .build();

        long count = 2;
        double percentage = 0.12;
        DensityAlert alert = DensityAlert.builder()
                .result(result)
                .count(count)
                .percentage(percentage)
                .build();

        Message<DensityAlert> message = MessageBuilder.withPayload(alert).build();
        Flux<Message<DensityAlert>> flux = Flux.just(message);
        consumerConfig.consumer().accept(flux);

        await()
                .pollDelay(ofSeconds(2L))
                .pollInterval(ofSeconds(2L))
                .atMost(ofSeconds(5L))
                .untilAsserted(() -> Assertions.assertThat(actual.get()).isEqualTo(alert));
    }
}