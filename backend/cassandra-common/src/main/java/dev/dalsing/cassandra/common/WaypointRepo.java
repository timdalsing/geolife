package dev.dalsing.cassandra.common;

import org.springframework.data.domain.Limit;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface WaypointRepo extends ReactiveCrudRepository<CassandraWaypoint, CassandraWaypointKey> {

    Flux<CassandraWaypoint> findByKeyPersonId(int personId, Limit limit);
    Flux<CassandraWaypoint> findByKeyPersonIdAndKeyTrajectoryId(int personId, String trajectoryId, Limit limit);


}
