package dev.dalsing.cassandra.common;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.domain.Limit;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface PersonTrajectoryRepo extends ReactiveCrudRepository<CassandraPersonTrajectory, CassandraPersonTrajectoryKey> {

    @Query("select distinct \"personId\" from person_trajectory")
    Flux<CassandraPersonTrajectory> findAllPersonIds();

    @Query("select \"trajectoryId\" from person_trajectory where \"personId\" = ?")
    Flux<CassandraPersonTrajectory> findTrajectoryIdByPersonId(int personId, Limit limit);
}
