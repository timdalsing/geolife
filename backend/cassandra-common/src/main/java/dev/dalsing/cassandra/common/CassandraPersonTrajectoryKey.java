package dev.dalsing.cassandra.common;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import static org.springframework.data.cassandra.core.cql.PrimaryKeyType.CLUSTERED;
import static org.springframework.data.cassandra.core.cql.PrimaryKeyType.PARTITIONED;

@PrimaryKeyClass
@Data
@Builder
public class CassandraPersonTrajectoryKey {

    @PrimaryKeyColumn(name = "personId", ordinal = 0, type = PARTITIONED)
    int personId;
    @PrimaryKeyColumn(name = "trajectoryId", ordinal = 1, type = CLUSTERED)
    String trajectoryId;
}
