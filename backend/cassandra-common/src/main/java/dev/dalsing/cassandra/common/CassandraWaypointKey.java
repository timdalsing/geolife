package dev.dalsing.cassandra.common;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import java.io.Serializable;

import static org.springframework.data.cassandra.core.cql.PrimaryKeyType.CLUSTERED;
import static org.springframework.data.cassandra.core.cql.PrimaryKeyType.PARTITIONED;

@PrimaryKeyClass
@Data
@Builder
public class CassandraWaypointKey implements Serializable {

    @PrimaryKeyColumn(name = "personId", ordinal = 0, type = PARTITIONED)
    int personId;
    @PrimaryKeyColumn(name = "trajectoryId", ordinal = 1, type = CLUSTERED)
    String trajectoryId;
    @PrimaryKeyColumn(name = "id", ordinal = 2, type = CLUSTERED)
    String id;
}
