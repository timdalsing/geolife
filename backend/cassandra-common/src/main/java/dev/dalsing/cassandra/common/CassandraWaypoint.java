package dev.dalsing.cassandra.common;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDateTime;

@Table("waypoint")
@Data
@Builder
public class CassandraWaypoint {

    @PrimaryKey("key")
    CassandraWaypointKey key;
    float lat;
    float lng;
    int altitude;
    LocalDateTime timestamp;
}
