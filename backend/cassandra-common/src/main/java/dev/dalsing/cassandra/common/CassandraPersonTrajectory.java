package dev.dalsing.cassandra.common;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("person_trajectory")
@Data
@Builder
public class CassandraPersonTrajectory {

    @PrimaryKey("key")
    CassandraPersonTrajectoryKey key;
}
