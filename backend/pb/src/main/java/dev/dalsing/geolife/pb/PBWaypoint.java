// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: src/main/proto/geolife.proto

package dev.dalsing.geolife.pb;

/**
 * Protobuf type {@code geolife.PBWaypoint}
 */
public final class PBWaypoint extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:geolife.PBWaypoint)
    PBWaypointOrBuilder {
private static final long serialVersionUID = 0L;
  // Use PBWaypoint.newBuilder() to construct.
  private PBWaypoint(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private PBWaypoint() {
    trajectoryId_ = "";
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new PBWaypoint();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private PBWaypoint(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 8: {

            personId_ = input.readInt32();
            break;
          }
          case 18: {
            java.lang.String s = input.readStringRequireUtf8();

            trajectoryId_ = s;
            break;
          }
          case 29: {

            lat_ = input.readFloat();
            break;
          }
          case 37: {

            lng_ = input.readFloat();
            break;
          }
          case 40: {

            altitude_ = input.readInt32();
            break;
          }
          case 48: {

            timestamp_ = input.readInt64();
            break;
          }
          default: {
            if (!parseUnknownField(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return dev.dalsing.geolife.pb.GeolifeProtos.internal_static_geolife_PBWaypoint_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return dev.dalsing.geolife.pb.GeolifeProtos.internal_static_geolife_PBWaypoint_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            dev.dalsing.geolife.pb.PBWaypoint.class, dev.dalsing.geolife.pb.PBWaypoint.Builder.class);
  }

  public static final int PERSON_ID_FIELD_NUMBER = 1;
  private int personId_;
  /**
   * <code>int32 person_id = 1;</code>
   * @return The personId.
   */
  @java.lang.Override
  public int getPersonId() {
    return personId_;
  }

  public static final int TRAJECTORY_ID_FIELD_NUMBER = 2;
  private volatile java.lang.Object trajectoryId_;
  /**
   * <code>string trajectory_id = 2;</code>
   * @return The trajectoryId.
   */
  @java.lang.Override
  public java.lang.String getTrajectoryId() {
    java.lang.Object ref = trajectoryId_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      trajectoryId_ = s;
      return s;
    }
  }
  /**
   * <code>string trajectory_id = 2;</code>
   * @return The bytes for trajectoryId.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getTrajectoryIdBytes() {
    java.lang.Object ref = trajectoryId_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      trajectoryId_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int LAT_FIELD_NUMBER = 3;
  private float lat_;
  /**
   * <code>float lat = 3;</code>
   * @return The lat.
   */
  @java.lang.Override
  public float getLat() {
    return lat_;
  }

  public static final int LNG_FIELD_NUMBER = 4;
  private float lng_;
  /**
   * <code>float lng = 4;</code>
   * @return The lng.
   */
  @java.lang.Override
  public float getLng() {
    return lng_;
  }

  public static final int ALTITUDE_FIELD_NUMBER = 5;
  private int altitude_;
  /**
   * <code>int32 altitude = 5;</code>
   * @return The altitude.
   */
  @java.lang.Override
  public int getAltitude() {
    return altitude_;
  }

  public static final int TIMESTAMP_FIELD_NUMBER = 6;
  private long timestamp_;
  /**
   * <code>int64 timestamp = 6;</code>
   * @return The timestamp.
   */
  @java.lang.Override
  public long getTimestamp() {
    return timestamp_;
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (personId_ != 0) {
      output.writeInt32(1, personId_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(trajectoryId_)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 2, trajectoryId_);
    }
    if (java.lang.Float.floatToRawIntBits(lat_) != 0) {
      output.writeFloat(3, lat_);
    }
    if (java.lang.Float.floatToRawIntBits(lng_) != 0) {
      output.writeFloat(4, lng_);
    }
    if (altitude_ != 0) {
      output.writeInt32(5, altitude_);
    }
    if (timestamp_ != 0L) {
      output.writeInt64(6, timestamp_);
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (personId_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt32Size(1, personId_);
    }
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(trajectoryId_)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, trajectoryId_);
    }
    if (java.lang.Float.floatToRawIntBits(lat_) != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeFloatSize(3, lat_);
    }
    if (java.lang.Float.floatToRawIntBits(lng_) != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeFloatSize(4, lng_);
    }
    if (altitude_ != 0) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt32Size(5, altitude_);
    }
    if (timestamp_ != 0L) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt64Size(6, timestamp_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof dev.dalsing.geolife.pb.PBWaypoint)) {
      return super.equals(obj);
    }
    dev.dalsing.geolife.pb.PBWaypoint other = (dev.dalsing.geolife.pb.PBWaypoint) obj;

    if (getPersonId()
        != other.getPersonId()) return false;
    if (!getTrajectoryId()
        .equals(other.getTrajectoryId())) return false;
    if (java.lang.Float.floatToIntBits(getLat())
        != java.lang.Float.floatToIntBits(
            other.getLat())) return false;
    if (java.lang.Float.floatToIntBits(getLng())
        != java.lang.Float.floatToIntBits(
            other.getLng())) return false;
    if (getAltitude()
        != other.getAltitude()) return false;
    if (getTimestamp()
        != other.getTimestamp()) return false;
    if (!unknownFields.equals(other.unknownFields)) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + PERSON_ID_FIELD_NUMBER;
    hash = (53 * hash) + getPersonId();
    hash = (37 * hash) + TRAJECTORY_ID_FIELD_NUMBER;
    hash = (53 * hash) + getTrajectoryId().hashCode();
    hash = (37 * hash) + LAT_FIELD_NUMBER;
    hash = (53 * hash) + java.lang.Float.floatToIntBits(
        getLat());
    hash = (37 * hash) + LNG_FIELD_NUMBER;
    hash = (53 * hash) + java.lang.Float.floatToIntBits(
        getLng());
    hash = (37 * hash) + ALTITUDE_FIELD_NUMBER;
    hash = (53 * hash) + getAltitude();
    hash = (37 * hash) + TIMESTAMP_FIELD_NUMBER;
    hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
        getTimestamp());
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static dev.dalsing.geolife.pb.PBWaypoint parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static dev.dalsing.geolife.pb.PBWaypoint parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(dev.dalsing.geolife.pb.PBWaypoint prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code geolife.PBWaypoint}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:geolife.PBWaypoint)
      dev.dalsing.geolife.pb.PBWaypointOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return dev.dalsing.geolife.pb.GeolifeProtos.internal_static_geolife_PBWaypoint_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return dev.dalsing.geolife.pb.GeolifeProtos.internal_static_geolife_PBWaypoint_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              dev.dalsing.geolife.pb.PBWaypoint.class, dev.dalsing.geolife.pb.PBWaypoint.Builder.class);
    }

    // Construct using dev.dalsing.geolife.pb.PBWaypoint.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      personId_ = 0;

      trajectoryId_ = "";

      lat_ = 0F;

      lng_ = 0F;

      altitude_ = 0;

      timestamp_ = 0L;

      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return dev.dalsing.geolife.pb.GeolifeProtos.internal_static_geolife_PBWaypoint_descriptor;
    }

    @java.lang.Override
    public dev.dalsing.geolife.pb.PBWaypoint getDefaultInstanceForType() {
      return dev.dalsing.geolife.pb.PBWaypoint.getDefaultInstance();
    }

    @java.lang.Override
    public dev.dalsing.geolife.pb.PBWaypoint build() {
      dev.dalsing.geolife.pb.PBWaypoint result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public dev.dalsing.geolife.pb.PBWaypoint buildPartial() {
      dev.dalsing.geolife.pb.PBWaypoint result = new dev.dalsing.geolife.pb.PBWaypoint(this);
      result.personId_ = personId_;
      result.trajectoryId_ = trajectoryId_;
      result.lat_ = lat_;
      result.lng_ = lng_;
      result.altitude_ = altitude_;
      result.timestamp_ = timestamp_;
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof dev.dalsing.geolife.pb.PBWaypoint) {
        return mergeFrom((dev.dalsing.geolife.pb.PBWaypoint)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(dev.dalsing.geolife.pb.PBWaypoint other) {
      if (other == dev.dalsing.geolife.pb.PBWaypoint.getDefaultInstance()) return this;
      if (other.getPersonId() != 0) {
        setPersonId(other.getPersonId());
      }
      if (!other.getTrajectoryId().isEmpty()) {
        trajectoryId_ = other.trajectoryId_;
        onChanged();
      }
      if (other.getLat() != 0F) {
        setLat(other.getLat());
      }
      if (other.getLng() != 0F) {
        setLng(other.getLng());
      }
      if (other.getAltitude() != 0) {
        setAltitude(other.getAltitude());
      }
      if (other.getTimestamp() != 0L) {
        setTimestamp(other.getTimestamp());
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      dev.dalsing.geolife.pb.PBWaypoint parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (dev.dalsing.geolife.pb.PBWaypoint) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }

    private int personId_ ;
    /**
     * <code>int32 person_id = 1;</code>
     * @return The personId.
     */
    @java.lang.Override
    public int getPersonId() {
      return personId_;
    }
    /**
     * <code>int32 person_id = 1;</code>
     * @param value The personId to set.
     * @return This builder for chaining.
     */
    public Builder setPersonId(int value) {
      
      personId_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>int32 person_id = 1;</code>
     * @return This builder for chaining.
     */
    public Builder clearPersonId() {
      
      personId_ = 0;
      onChanged();
      return this;
    }

    private java.lang.Object trajectoryId_ = "";
    /**
     * <code>string trajectory_id = 2;</code>
     * @return The trajectoryId.
     */
    public java.lang.String getTrajectoryId() {
      java.lang.Object ref = trajectoryId_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        trajectoryId_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string trajectory_id = 2;</code>
     * @return The bytes for trajectoryId.
     */
    public com.google.protobuf.ByteString
        getTrajectoryIdBytes() {
      java.lang.Object ref = trajectoryId_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        trajectoryId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string trajectory_id = 2;</code>
     * @param value The trajectoryId to set.
     * @return This builder for chaining.
     */
    public Builder setTrajectoryId(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      trajectoryId_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string trajectory_id = 2;</code>
     * @return This builder for chaining.
     */
    public Builder clearTrajectoryId() {
      
      trajectoryId_ = getDefaultInstance().getTrajectoryId();
      onChanged();
      return this;
    }
    /**
     * <code>string trajectory_id = 2;</code>
     * @param value The bytes for trajectoryId to set.
     * @return This builder for chaining.
     */
    public Builder setTrajectoryIdBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      trajectoryId_ = value;
      onChanged();
      return this;
    }

    private float lat_ ;
    /**
     * <code>float lat = 3;</code>
     * @return The lat.
     */
    @java.lang.Override
    public float getLat() {
      return lat_;
    }
    /**
     * <code>float lat = 3;</code>
     * @param value The lat to set.
     * @return This builder for chaining.
     */
    public Builder setLat(float value) {
      
      lat_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>float lat = 3;</code>
     * @return This builder for chaining.
     */
    public Builder clearLat() {
      
      lat_ = 0F;
      onChanged();
      return this;
    }

    private float lng_ ;
    /**
     * <code>float lng = 4;</code>
     * @return The lng.
     */
    @java.lang.Override
    public float getLng() {
      return lng_;
    }
    /**
     * <code>float lng = 4;</code>
     * @param value The lng to set.
     * @return This builder for chaining.
     */
    public Builder setLng(float value) {
      
      lng_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>float lng = 4;</code>
     * @return This builder for chaining.
     */
    public Builder clearLng() {
      
      lng_ = 0F;
      onChanged();
      return this;
    }

    private int altitude_ ;
    /**
     * <code>int32 altitude = 5;</code>
     * @return The altitude.
     */
    @java.lang.Override
    public int getAltitude() {
      return altitude_;
    }
    /**
     * <code>int32 altitude = 5;</code>
     * @param value The altitude to set.
     * @return This builder for chaining.
     */
    public Builder setAltitude(int value) {
      
      altitude_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>int32 altitude = 5;</code>
     * @return This builder for chaining.
     */
    public Builder clearAltitude() {
      
      altitude_ = 0;
      onChanged();
      return this;
    }

    private long timestamp_ ;
    /**
     * <code>int64 timestamp = 6;</code>
     * @return The timestamp.
     */
    @java.lang.Override
    public long getTimestamp() {
      return timestamp_;
    }
    /**
     * <code>int64 timestamp = 6;</code>
     * @param value The timestamp to set.
     * @return This builder for chaining.
     */
    public Builder setTimestamp(long value) {
      
      timestamp_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>int64 timestamp = 6;</code>
     * @return This builder for chaining.
     */
    public Builder clearTimestamp() {
      
      timestamp_ = 0L;
      onChanged();
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:geolife.PBWaypoint)
  }

  // @@protoc_insertion_point(class_scope:geolife.PBWaypoint)
  private static final dev.dalsing.geolife.pb.PBWaypoint DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new dev.dalsing.geolife.pb.PBWaypoint();
  }

  public static dev.dalsing.geolife.pb.PBWaypoint getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<PBWaypoint>
      PARSER = new com.google.protobuf.AbstractParser<PBWaypoint>() {
    @java.lang.Override
    public PBWaypoint parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new PBWaypoint(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<PBWaypoint> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<PBWaypoint> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public dev.dalsing.geolife.pb.PBWaypoint getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

