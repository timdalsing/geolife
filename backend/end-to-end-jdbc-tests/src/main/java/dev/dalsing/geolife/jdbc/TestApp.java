package dev.dalsing.geolife.jdbc;

import dev.dalsing.geolife.common.ObjectMapperConfig;
import dev.dalsing.geolife.pb.common.WaypointMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({WaypointMessageConverter.class, ObjectMapperConfig.class})
public class TestApp {

    public static void main(String[] args) {
        SpringApplication.run(TestApp.class, args);
    }
}
