package dev.dalsing.geolife.jdbc;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.pb.PBWaypoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.MimeType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static java.time.Duration.ofSeconds;
import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@SpringBootTest
class JdbcTest {

    @Autowired
    StreamBridge bridge;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @BeforeEach
    void setUp() {
        jdbcTemplate.update("delete from waypoint");
    }

    @AfterEach
    void tearDown() {
        jdbcTemplate.update("delete from waypoint");
    }

    @Tag("integration")
    @Test
    void jdbc_success() {
        int personId = 1;
        String trajectoryId = UUID.randomUUID().toString();
        float lat = 1.23f;
        float lng = 2.34f;
        int alt = 100;
        LocalDateTime now = LocalDateTime.now().truncatedTo(MILLIS);
        long ts = now.toInstant(UTC).toEpochMilli();

        PBWaypoint pbWaypoint = PBWaypoint.newBuilder()
                .setPersonId(personId)
                .setTrajectoryId(trajectoryId)
                .setLat(lat)
                .setLng(lng)
                .setAltitude(alt)
                .setTimestamp(ts)
                .build();

        bridge.send("pb-out-0", pbWaypoint, MimeType.valueOf("application/x-protobuf"));

        await()
                .pollDelay(ofSeconds(2L))
                .pollInterval(ofSeconds(2L))
                .atMost(ofSeconds(10L))
                .untilAsserted(() -> {
                    List<GeolifeWaypoint> actualList = jdbcTemplate
                            .query("select * from waypoint where person_id = ?",
                                    (rs, rowNum) -> GeolifeWaypoint.builder()
                                            .id(rs.getString(1))
                                            .personId(rs.getInt(2))
                                            .trajectoryId(rs.getString(3))
                                            .lat(rs.getFloat(4))
                                            .lng(rs.getFloat(5))
                                            .altitude(rs.getInt(6))
                                            .timestamp(rs.getTimestamp(7).toLocalDateTime())
                                            .build(),
                                    personId);
                    assertThat(actualList).hasSize(1);
                    GeolifeWaypoint actualWaypoint = actualList.get(0);
                    assertThat(actualWaypoint.getPersonId()).isEqualTo(personId);
                    assertThat(actualWaypoint.getTrajectoryId()).isEqualTo(trajectoryId);
                    assertThat(actualWaypoint.getLat()).isEqualTo(lat);
                    assertThat(actualWaypoint.getLng()).isEqualTo(lng);
                    assertThat(actualWaypoint.getAltitude()).isEqualTo(alt);
                    assertThat(actualWaypoint.getTimestamp()).isEqualTo(now);
                });
    }
}