package dev.dalsing.geolife.density.processor;

import dev.dalsing.geolife.common.ObjectMapperConfig;
import dev.dalsing.geolife.model.DensityResult;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import reactor.core.publisher.Flux;
import smile.clustering.KMeans;

import java.util.function.Function;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@Import({ObjectMapperConfig.class})
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class DensityProcessorApp {

    KMeans kMeans;
    Timer timer;

    public static void main(String[] args) {
        SpringApplication.run(DensityProcessorApp.class, args);
    }

    @Bean
    public static Timer timer(MeterRegistry registry) {
        return registry.timer("density-processor-timer");
    }

    @Bean
    public Function<Flux<GeolifeWaypoint>, Flux<Message<DensityResult>>> process() {
     return flux -> flux.map(wp -> {
         double lng = wp.getLng();
         double lat = wp.getLat();
         double[] x = {lng,lat};
         int cluster = timer.record(() -> kMeans.predict(x));
         double[][] centroids = kMeans.centroids;
         double[] centroid = centroids[cluster];
         DensityResult result = DensityResult.builder()
                 .waypoint(wp)
                 .centroid(centroid)
                 .cluster(cluster)
                 .build();
         return MessageBuilder.withPayload(result).setHeader("partitionKey", cluster).build();
     });
    }
}
