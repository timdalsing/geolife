package dev.dalsing.geolife.density.processor;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.File;

@ConfigurationProperties("app")
@Data
public class AppProperties {

    File trainingDir = new File("/Users/tdalsing/workspace/projects/geolife/misc/data/geolife/sample");
    int numberOfClusters = 100;
}
