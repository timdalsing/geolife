package dev.dalsing.geolife.density.processor;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import smile.clustering.KMeans;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@Configuration
@EnableConfigurationProperties(AppProperties.class)
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class SmileConfig {

    AppProperties appProperties;

    @Bean
    public KMeans kMeans() throws Exception {
        double[][] trainingData = loadTrainingData();
        return KMeans.fit(trainingData, appProperties.getNumberOfClusters());
    }

    private double[][] loadTrainingData() throws Exception {
        List<double[]> list = new ArrayList<>();
        loadTrainingData(appProperties.getTrainingDir(), list);
        return list.toArray(double[][]::new);
    }

    private void loadTrainingData(File file, List<double[]> list) throws Exception {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File sfile : files) {
                loadTrainingData(sfile, list);
            }
        } else if (file.isFile() && file.getName().toLowerCase().endsWith(".plt")) {
            log.info("loadTrainingData: file = {}", file.getAbsolutePath());
            try (FileReader fileReader = new FileReader(file)) {
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                bufferedReader.readLine();
                bufferedReader.readLine();
                bufferedReader.readLine();
                bufferedReader.readLine();
                bufferedReader.readLine();
                bufferedReader.readLine();

                String line;

                while ((line = bufferedReader.readLine()) != null) {
                    String[] fields = line.split(",");

                    double lat = Float.parseFloat(fields[0]);
                    double lng = Float.parseFloat(fields[1]);

                    double[] x = {lng, lat};
                    list.add(x);
                }
            }
        }
    }
}
