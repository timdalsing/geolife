package dev.dalsing.geolife.density.processor;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import smile.clustering.KMeans;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

@Slf4j
public class SmileTest {

    @Test
    void smile() throws Exception {
        log.info("Training...");
        Map<Integer, Integer> predictions = new HashMap<>();
        KMeans kMeans;

        try(FileReader reader = new FileReader("/Users/tdalsing/workspace/projects/geolife/misc/data/geolife/sample/000/Trajectory/20081023025304.plt")) {
            List<double[]> data = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(reader);

            bufferedReader.readLine();
            bufferedReader.readLine();
            bufferedReader.readLine();
            bufferedReader.readLine();
            bufferedReader.readLine();
            bufferedReader.readLine();

            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] fields = line.split(",");

                double lat = Float.parseFloat(fields[0]);
                double lng = Float.parseFloat(fields[1]);

                double[] x = {lng,lat};
                data.add(x);
            }

            double[][] sample = data.toArray(double[][]::new);
            kMeans = KMeans.fit(sample, 100);
        }

        log.info("Predicting...");
        try(FileReader reader = new FileReader("/Users/tdalsing/workspace/projects/geolife/misc/data/geolife/sample/000/Trajectory/20081024020959.plt")) {
            BufferedReader bufferedReader = new BufferedReader(reader);

            bufferedReader.readLine();
            bufferedReader.readLine();
            bufferedReader.readLine();
            bufferedReader.readLine();
            bufferedReader.readLine();
            bufferedReader.readLine();

            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] fields = line.split(",");

                double lat = Float.parseFloat(fields[0]);
                double lng = Float.parseFloat(fields[1]);

                double[] x = {lng,lat};
                int predict = kMeans.predict(x);
                double[] centroid = kMeans.centroids[predict];
                log.info("Predicting: predict = {}, centroid = {}", predict, Arrays.toString(centroid));


                Integer count = predictions.computeIfAbsent(predict, (t) -> 0);
                predictions.put(predict, ++count);
            }
        }

        log.info("predictions = {}", predictions);
    }
}
