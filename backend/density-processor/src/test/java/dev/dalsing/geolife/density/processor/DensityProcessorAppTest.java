package dev.dalsing.geolife.density.processor;

import dev.dalsing.geolife.model.DensityResult;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import io.micrometer.core.instrument.Timer;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.Message;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;
import smile.clustering.KMeans;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.function.IntSupplier;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@Slf4j
class DensityProcessorAppTest {

    DensityProcessorApp app;

    @Mock
    KMeans kMeans;

    @Mock
    Timer timer;

    @BeforeEach
    void setUp() {
        app = new DensityProcessorApp(kMeans, timer);
    }

    @Test
    void process_success() throws Exception {
        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        int cluster = 0;
        double[] centroid = {1.0, 2.0};
        setCentroids(centroid); // workaround for final field in mock
        DensityResult expected = DensityResult.builder()
                .waypoint(waypoint)
                .cluster(cluster)
                .centroid(centroid)
                .build();

        Flux<GeolifeWaypoint> waypointFlux = Flux.just(waypoint);

        Mockito.doAnswer(invocation -> {
            IntSupplier supplier = invocation.getArgument(0);
            return supplier.getAsInt();
        }).when(timer).record(Mockito.any(IntSupplier.class));

        Flux<Message<DensityResult>> resultFlux = app.process().apply(waypointFlux);

        StepVerifier.create(resultFlux).assertNext(actual -> {
            assertThat(actual.getPayload()).isEqualTo(expected);
            assertThat(actual.getHeaders().get("partitionKey", Integer.class)).isEqualTo(cluster);
        }).verifyComplete();
    }

    private void setCentroids(double[] centroid) throws Exception {
        double[][] t = new double[1][1];
        t[0] = centroid;
        Field centroids = kMeans.getClass().getField("centroids");
        centroids.setAccessible(true);
        centroids.set(kMeans, t);
    }
}