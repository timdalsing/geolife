package dev.dalsing.geolife.density.analysis;

import dev.dalsing.geolife.common.ObjectMapperConfig;
import dev.dalsing.geolife.model.DensityAlert;
import dev.dalsing.geolife.model.DensityResult;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@Import({ObjectMapperConfig.class})
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class DensityAnalysisApp {

    JdbcTemplate jdbcTemplate;
    Timer timer;

    public static void main(String[] args) {
        SpringApplication.run(DensityAnalysisApp.class, args);
    }

    @Bean
    public static Timer timer(MeterRegistry registry) {
        return registry.timer("density-analysis-timer");
    }

    @Bean
    public Function<Flux<DensityResult>, Flux<DensityAlert>> process() {
        return flux -> flux.map(this::check);
    }

    private DensityAlert check(DensityResult result) {
        return timer.record(() -> _check(result));
    }

    private DensityAlert _check(DensityResult result) {
        int cluster = result.getCluster();
        List<Percentage> percentages = jdbcTemplate.query("select cluster, density_count from density_percentage",
                (rs, rowNum) -> Percentage.builder()
                        .cluster(rs.getInt(1))
                        .count(rs.getLong(2))
                        .build());
        long newTotal = percentages.stream().mapToLong(p -> p.getCount()).sum() + 1;
        List<Percentage> filtered = percentages.stream().filter(p -> p.getCluster() == cluster).collect(Collectors.toList());
        double newPercent = 0.0;
        long newCount = 1;

        if (filtered.isEmpty()) {
            newPercent = 1.0 / newTotal;
            jdbcTemplate.update("insert into density_percentage (cluster, density_count) values (?,?)", cluster, 1L);
        } else if (filtered.size() == 1) {
            Percentage currentPercent = filtered.get(0);
            newCount = currentPercent.getCount() + 1;
            newPercent = 1.0 * newCount / newTotal;
            jdbcTemplate.update("update density_percentage set density_count = ? where cluster = ?", newCount, cluster);
        } else {
            log.error("check: should never get here: cluster = {}", cluster);
            return null;
        }

        return DensityAlert.builder()
                .result(result)
                .percentage(newPercent)
                .count(newCount)
                .build();
    }
}
