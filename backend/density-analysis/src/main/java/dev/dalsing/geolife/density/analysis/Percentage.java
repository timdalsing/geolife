package dev.dalsing.geolife.density.analysis;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Percentage {

    int cluster;
    long count;
}
