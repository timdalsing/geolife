package dev.dalsing.geolife.density.analysis;

import dev.dalsing.geolife.model.DensityAlert;
import dev.dalsing.geolife.model.DensityResult;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import io.micrometer.core.instrument.Timer;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Supplier;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DensityAnalysisAppTest {

    DensityAnalysisApp app;

    @Mock
    JdbcTemplate jdbcTemplate;

    @Mock
    Timer timer;

    @BeforeEach
    void setUp() {
        app = new DensityAnalysisApp(jdbcTemplate, timer);
    }

    @Test
    void process_normal() {
        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        int cluster = 0;
        double[] centroid = {1.0, 2.0};

        DensityResult result = DensityResult.builder()
                .waypoint(waypoint)
                .centroid(centroid)
                .cluster(cluster)
                .build();

        DensityAlert expected = DensityAlert.builder()
                .result(result)
                .percentage(0.3)
                .count(3L)
                .build();

        Percentage percent0 = Percentage.builder()
                .cluster(0)
                .count(2)
                .build();

        Percentage percent1 = Percentage.builder()
                .cluster(1)
                .count(3)
                .build();

        Percentage percent2 = Percentage.builder()
                .cluster(2)
                .count(4)
                .build();

        List<Percentage> percentages = List.of(percent0, percent1, percent2);
        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(percentages);

        doAnswer(invocation ->  {
            Supplier supplier = invocation.getArgument(0);
            return supplier.get();
        }).when(timer).record(any(Supplier.class));

        Flux<DensityAlert> alertFlux = app.process().apply(Flux.just(result));

        StepVerifier.create(alertFlux).assertNext(actual -> Assertions.assertThat(actual).isEqualTo(expected)).verifyComplete();

        Mockito.verify(jdbcTemplate).update("update density_percentage set density_count = ? where cluster = ?", 3L, 0);
    }

    @Test
    void process_insert() {
        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        int cluster = 0;
        double[] centroid = {1.0, 2.0};

        DensityResult result = DensityResult.builder()
                .waypoint(waypoint)
                .centroid(centroid)
                .cluster(cluster)
                .build();

        DensityAlert expected = DensityAlert.builder()
                .result(result)
                .percentage(0.1)
                .count(1L)
                .build();

        Percentage percent1 = Percentage.builder()
                .cluster(1)
                .count(4)
                .build();

        Percentage percent2 = Percentage.builder()
                .cluster(2)
                .count(5)
                .build();

        List<Percentage> percentages = List.of(percent1, percent2);
        when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(percentages);

        doAnswer(invocation ->  {
            Supplier supplier = invocation.getArgument(0);
            return supplier.get();
        }).when(timer).record(any(Supplier.class));

        Flux<DensityAlert> alertFlux = app.process().apply(Flux.just(result));

        StepVerifier.create(alertFlux).assertNext(actual -> Assertions.assertThat(actual).isEqualTo(expected)).verifyComplete();

        Mockito.verify(jdbcTemplate).update("insert into density_percentage (cluster, density_count) values (?,?)", 0, 1L);
    }
}