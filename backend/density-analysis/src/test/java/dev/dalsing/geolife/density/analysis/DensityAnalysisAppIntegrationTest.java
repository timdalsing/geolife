package dev.dalsing.geolife.density.analysis;

import dev.dalsing.geolife.model.DensityAlert;
import dev.dalsing.geolife.model.DensityResult;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;

import static java.time.temporal.ChronoUnit.MILLIS;

@SpringBootTest
class DensityAnalysisAppIntegrationTest {

    @Autowired
    DensityAnalysisApp app;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @BeforeEach
    void setUp() {
        jdbcTemplate.update("delete from density_percentage");
    }

    @AfterEach
    void tearDown() {
        jdbcTemplate.update("delete from density_percentage");
    }

    @Tag("integration")
    @Test
    void process_noRows() {
        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        int cluster = 0;
        double[] centroid = {1.0, 2.0};

        DensityResult result = DensityResult.builder()
                .waypoint(waypoint)
                .centroid(centroid)
                .cluster(cluster)
                .build();

        Flux<DensityResult> resultFlux = Flux.just(result);

        DensityAlert expected = DensityAlert.builder()
                .result(result)
                .percentage(1.0)
                .count(1L)
                .build();

        Flux<DensityAlert> alertFlux = app.process().apply(resultFlux);
        StepVerifier.create(alertFlux).assertNext(actual -> Assertions.assertThat(actual).isEqualTo(expected)).verifyComplete();
    }

    @Tag("integration")
    @Test
    void process_existingRow() {
        jdbcTemplate.update("insert into density_percentage (cluster, density_count) values (0, 9)");

        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        int cluster = 0;
        double[] centroid = {1.0, 2.0};

        DensityResult result = DensityResult.builder()
                .waypoint(waypoint)
                .centroid(centroid)
                .cluster(cluster)
                .build();

        Flux<DensityResult> resultFlux = Flux.just(result);

        DensityAlert expected = DensityAlert.builder()
                .result(result)
                .percentage(1.0)
                .count(10L)
                .build();

        Flux<DensityAlert> alertFlux = app.process().apply(resultFlux);
        StepVerifier.create(alertFlux).assertNext(actual -> Assertions.assertThat(actual).isEqualTo(expected)).verifyComplete();
    }

    @Tag("integration")
    @Test
    void process_missingRow() {
        jdbcTemplate.update("insert into density_percentage (cluster, density_count) values (1, 4)");
        jdbcTemplate.update("insert into density_percentage (cluster, density_count) values (2, 5)");

        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        int cluster = 0;
        double[] centroid = {1.0, 2.0};

        DensityResult result = DensityResult.builder()
                .waypoint(waypoint)
                .centroid(centroid)
                .cluster(cluster)
                .build();

        Flux<DensityResult> resultFlux = Flux.just(result);

        DensityAlert expected = DensityAlert.builder()
                .result(result)
                .percentage(0.1)
                .count(1L)
                .build();

        Flux<DensityAlert> alertFlux = app.process().apply(resultFlux);
        StepVerifier.create(alertFlux).assertNext(actual -> Assertions.assertThat(actual).isEqualTo(expected)).verifyComplete();
    }
}