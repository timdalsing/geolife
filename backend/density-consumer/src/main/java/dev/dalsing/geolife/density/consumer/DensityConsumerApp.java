package dev.dalsing.geolife.density.consumer;

import dev.dalsing.geolife.common.ObjectMapperConfig;
import dev.dalsing.geolife.model.DensityResult;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Flux;

import java.util.function.Consumer;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@Import({ObjectMapperConfig.class})
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class DensityConsumerApp {

    JdbcTemplate jdbcTemplate;
    Timer timer;

    public static void main(String[] args) {
        SpringApplication.run(DensityConsumerApp.class, args);
    }

    @Bean
    public static Timer timer(MeterRegistry registry) {
        return registry.timer("density-consumer-timer");
    }

    @Bean
    public Consumer<Flux<DensityResult>> consumer() {
        return flux -> flux.subscribe(result -> {
            GeolifeWaypoint waypoint = result.getWaypoint();
            double[] centroid = result.getCentroid();
            timer.record(() ->
                    jdbcTemplate.update("insert into density_result (waypoint_id, cluster, centroid_lng, centroid_lat) values (?,?,?,?)",
                            waypoint.getId(), result.getCluster(), centroid[0], centroid[1]));
        });
    }
}
