package dev.dalsing.geolife.density.consumer;

import dev.dalsing.geolife.model.DensityResult;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DensityConsumerAppIntegrationTest {

    @Autowired
    DensityConsumerApp app;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @BeforeEach
    void setUp() {
    }

    @Tag("integration")
    @Test
    void consumer_success() {
        String id = UUID.randomUUID().toString();
        int personId = 1;
        String trajectoryId = UUID.randomUUID().toString();
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        int cluster = 0;
        double[] centroid = {1.0, 2.0};

        DensityResult result = DensityResult.builder()
                .waypoint(waypoint)
                .centroid(centroid)
                .cluster(cluster)
                .build();

        Flux<DensityResult> resultFlux = Flux.just(result);
        app.consumer().accept(resultFlux);

        List<DensityResult> actual = jdbcTemplate.query("select waypoint_id, cluster, centroid_lng, centroid_lat from density_result where waypoint_id = ?",
                (row, rowNum) -> DensityResult.builder()
                        .waypoint(waypoint)
                        .cluster(row.getInt(2))
                        .centroid(new double[]{row.getFloat(3), row.getFloat(4)})
                        .build(), id);

        assertThat(actual).hasSize(1);
        assertThat(actual).containsOnly(result);
    }
}