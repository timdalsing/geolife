package dev.dalsing.geolife.density.consumer;

import dev.dalsing.geolife.model.DensityResult;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import io.micrometer.core.instrument.Timer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Flux;

import java.time.LocalDateTime;
import java.util.function.IntSupplier;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DensityConsumerAppTest {

    DensityConsumerApp app;

    @Mock
    JdbcTemplate jdbcTemplate;

    @Mock
    Timer timer;

    @BeforeEach
    void setUp() {
        app = new DensityConsumerApp(jdbcTemplate, timer);
    }

    @Test
    void consumer() {
        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        int cluster = 0;
        double[] centroid = {1.0, 2.0};

        DensityResult result = DensityResult.builder()
                .waypoint(waypoint)
                .centroid(centroid)
                .cluster(cluster)
                .build();

        Mockito.doAnswer(invocation -> {
            IntSupplier supplier = invocation.getArgument(0);
            supplier.getAsInt();
            return null;
        }).when(timer).record(any(IntSupplier.class));

        Flux<DensityResult> resultFlux = Flux.just(result);
        app.consumer().accept(resultFlux);

        verify(jdbcTemplate)
                .update("insert into density_result (waypoint_id, cluster, centroid_lng, centroid_lat) values (?,?,?,?)",
                        id, cluster, 1.0, 2.0);
    }
}