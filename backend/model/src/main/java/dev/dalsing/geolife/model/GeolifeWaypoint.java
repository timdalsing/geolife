package dev.dalsing.geolife.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

import static com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GeolifeWaypoint {

    String id;
    int personId;
    String trajectoryId;
    float lat;
    float lng;
    int altitude;
    @JsonFormat(shape = STRING)
    LocalDateTime timestamp;
}
