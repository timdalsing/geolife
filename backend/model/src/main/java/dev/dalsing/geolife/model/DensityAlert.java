package dev.dalsing.geolife.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DensityAlert {

    DensityResult result;
    long count;
    double percentage;
}
