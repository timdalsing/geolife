package dev.dalsing.geolife.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.locationtech.jts.geom.MultiPolygon;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProximityAlert {

    GeolifeWaypoint waypoint;
    MultiPolygon boundaries;
    String locationName;
    String message;
}
