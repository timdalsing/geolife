package dev.dalsing.geolife.jdbc.service;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@RestController
@CrossOrigin
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class JdbcServiceApp {

    JdbcTemplate jdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(JdbcServiceApp.class, args);
    }

    @GetMapping("/personIds")
    public Mono<List<Integer>> getPersonIds() {
        List<Integer> list = jdbcTemplate.query("select distinct person_id from waypoint order by person_id asc", (rs, rowNum) -> rs.getInt("person_id"));
        return Mono.just(list);
    }

    @GetMapping("/trajectory/{personId}")
    public Mono<List<String>> findTrajectoryForPerson(@PathVariable("personId") int personId) {
        List<String> trajectoryIds = jdbcTemplate.query("select distinct trajectory_id from waypoint where person_id = ? order by trajectory_id",
                (rs, rowNum) -> rs.getString(1), personId);
        return Mono.just(trajectoryIds);
    }

    @GetMapping("/waypoint/{personId}")
    public Mono<ResponseEntity<List<GeolifeWaypoint>>> findWaypointsForPerson(
            @PathVariable("personId") int personId,
            @RequestParam(name = "limit", required = false, defaultValue = "100") int limit) {

        Assert.isTrue(limit >= 0 && limit <= 100, () -> String.format("Invalid limit must be 0 to 100 inclusive, provided %d", limit));
        String sql = limit == 0
                ? "select * from waypoint where person_id = ? order by ts asc"
                : String.format("select * from waypoint where person_id = ? order by ts asc limit %d", limit);

        List<GeolifeWaypoint> waypoints = jdbcTemplate
                .query(sql,
                        (rs, rowNum) -> GeolifeWaypoint.builder()
                                .id(rs.getString(1))
                                .personId(rs.getInt(2))
                                .trajectoryId(rs.getString(3))
                                .lat(rs.getFloat(4))
                                .lng(rs.getFloat(5))
                                .altitude(rs.getInt(6))
                                .timestamp(rs.getTimestamp(7).toLocalDateTime())
                                .build(),
                        personId);
        if (waypoints.isEmpty()) {
            return Mono.just(ResponseEntity.noContent().build());
        }

        return Mono.just(ResponseEntity.ok(waypoints));
    }

    @GetMapping("/waypoint/{personId}/{trajectoryId}")
    public Mono<ResponseEntity<List<GeolifeWaypoint>>> findWaypointsForPersonAndTrajectory(@PathVariable("personId") int personId,
                                                                                      @PathVariable("trajectoryId") String trajectoryId,
                                                                                      @RequestParam(name = "limit", required = false, defaultValue = "100") int limit) {

        Assert.isTrue(limit >= 0 && limit <= 100, () -> String.format("Invalid limit must be 0 to 100 inclusive, provided %d", limit));
        String sql = limit == 0
                ? "select * from waypoint where person_id = ? and trajectory_id = ? order by ts asc"
                : String.format("select * from waypoint where person_id = ? and trajectory_id = ? order by ts asc limit %d", limit);

        List<GeolifeWaypoint> waypoints = jdbcTemplate
                .query(sql,
                        (rs, rowNum) -> GeolifeWaypoint.builder()
                                .id(rs.getString(1))
                                .personId(rs.getInt(2))
                                .trajectoryId(rs.getString(3))
                                .lat(rs.getFloat(4))
                                .lng(rs.getFloat(5))
                                .altitude(rs.getInt(6))
                                .timestamp(rs.getTimestamp(7).toLocalDateTime())
                                .build(),
                        personId, trajectoryId);

        if (waypoints.isEmpty()) {
            return Mono.just(ResponseEntity.noContent().build());
        }

        return Mono.just(ResponseEntity.ok(waypoints));
    }
}
