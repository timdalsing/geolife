package dev.dalsing.geolife.jdbc.service;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JdbcServiceAppTest {

    JdbcServiceApp app;

    @Mock
    JdbcTemplate jdbcTemplate;

    static Random random = new Random();
    List<GeolifeWaypoint> waypoints;

    @BeforeEach
    void setUp() {
        app = new JdbcServiceApp(jdbcTemplate);
        waypoints = new ArrayList<>();
        for (int personId = 0; personId < 3; ++personId) {
            for (int trajectoryId = 0; trajectoryId < 4; ++trajectoryId) {
                for (int x = 0; x < 3; ++x) {
                    GeolifeWaypoint waypoint = createWaypoint(personId, "trajectory-" + trajectoryId);
                    waypoints.add(waypoint);
                }
            }
        }
    }

    @Test
    void findTrajectoryForPerson_success() {
        int personId = 1;
        List<String> expected = waypoints.stream().filter(wp -> wp.getPersonId() == personId).map(wp -> wp.getTrajectoryId()).distinct().sorted()
                .collect(Collectors.toList());
        when(jdbcTemplate.query(anyString(), any(RowMapper.class), any(Object.class))).thenReturn(expected);
        Mono<List<String>> trajectoryForPerson = app.findTrajectoryForPerson(personId);
        StepVerifier.create(trajectoryForPerson).assertNext(list -> Assertions.assertThat(list).containsAll(expected)).verifyComplete();
    }

    @Test
    void findWaypointsForPerson_success() {
        int personId = 1;
        int limit =  10;

        List<GeolifeWaypoint> expected = waypoints.stream().filter(wp -> wp.getPersonId() == personId).collect(Collectors.toList());

        when(jdbcTemplate.query(anyString(), any(RowMapper.class), anyInt())).thenReturn(expected);
        Mono<ResponseEntity<List<GeolifeWaypoint>>> waypointsForPerson = app.findWaypointsForPerson(personId, limit);
        StepVerifier.create(waypointsForPerson).assertNext(list -> Assertions.assertThat(list.getBody()).containsAll(expected)).verifyComplete();
    }

    @Test
    void findWaypointsForPersonAndTrajectory_success() {
        int personId = 1;
        String trajectoryId = "trajectory-1";
        int limit =  10;

        List<GeolifeWaypoint> expected = waypoints.stream().filter(wp -> wp.getPersonId() == personId && wp.getTrajectoryId().equals(trajectoryId))
                .collect(Collectors.toList());

        when(jdbcTemplate.query(anyString(), any(RowMapper.class), any(Object[].class))).thenReturn(expected);
        Mono<ResponseEntity<List<GeolifeWaypoint>>> actual = app.findWaypointsForPersonAndTrajectory(personId, trajectoryId, limit);
        StepVerifier.create(actual).assertNext(t -> Assertions.assertThat(t.getBody()).isEqualTo(expected)).verifyComplete();
    }

    private GeolifeWaypoint createWaypoint(int personId, String trajectoryId) {
        return GeolifeWaypoint.builder()
                .id(UUID.randomUUID().toString())
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(random.nextFloat())
                .lng(random.nextFloat())
                .altitude(random.nextInt())
                .timestamp(LocalDateTime.now().truncatedTo(MILLIS))
                .build();
    }

}