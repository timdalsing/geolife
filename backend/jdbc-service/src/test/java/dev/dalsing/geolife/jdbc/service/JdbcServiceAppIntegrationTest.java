package dev.dalsing.geolife.jdbc.service;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class JdbcServiceAppIntegrationTest {

    @Autowired
    JdbcServiceApp app;

    @Autowired
    JdbcTemplate jdbcTemplate;

    static Random random = new Random();
    List<GeolifeWaypoint> waypoints;

    @BeforeEach
    void setUp() {
        jdbcTemplate.update("delete from waypoint");

        waypoints = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now().truncatedTo(MILLIS);

        for (int personId = 0; personId < 3; ++personId) {
            for (int trajectoryId = 0; trajectoryId < 4; ++trajectoryId) {
                for (int x = 0; x < 3; ++x) {
                    GeolifeWaypoint waypoint = createWaypoint(personId, "trajectory-" + trajectoryId, now);
                    waypoints.add(waypoint);
                    now = now.plus(Duration.ofSeconds(1));
                }
            }
        }
        waypoints.forEach(wp -> jdbcTemplate.update("insert into waypoint (id,person_id,trajectory_id,lat,lng,altitude,ts) values (?,?,?,?,?,?,?)",
                wp.getId(), wp.getPersonId(), wp.getTrajectoryId(), wp.getLat(), wp.getLng(), wp.getAltitude(), Timestamp.valueOf(wp.getTimestamp())));
    }

    @AfterEach
    void tearDown() {
        jdbcTemplate.update("delete from waypoint");
    }

    @Test
    void getPersonIds_success() {
        Mono<List<Integer>> personIds = app.getPersonIds();
        StepVerifier.create(personIds).assertNext(l -> assertThat(l).containsExactlyInAnyOrder(0, 1, 2, 3)).expectComplete();
    }

    @Tag("integration")
    @Test
    void findTrajectoryForPerson_success() {
        int personId = 1;
        List<String> expected = waypoints.stream().filter(wp -> wp.getPersonId() == personId).map(wp -> wp.getTrajectoryId()).distinct().sorted()
                .collect(Collectors.toList());
        Mono<List<String>> trajectoryForPerson = app.findTrajectoryForPerson(personId);
        StepVerifier.create(trajectoryForPerson).assertNext(list -> assertThat(list).containsAll(expected)).verifyComplete();
    }

    @Tag("integration")
    @Test
    void findWaypointsForPerson_success() {
        int personId = 1;
        int limit = 10;

        List<GeolifeWaypoint> expected = waypoints.stream().filter(wp -> wp.getPersonId() == personId).limit(limit).collect(Collectors.toList());
        Mono<ResponseEntity<List<GeolifeWaypoint>>> waypointsForPerson = app.findWaypointsForPerson(personId, limit);
        StepVerifier.create(waypointsForPerson).assertNext(list -> assertThat(list.getBody()).containsAll(expected)).verifyComplete();
    }

    @Tag("integration")
    @Test
    void findWaypointsForPersonAndTrajectory_success() {
        int personId = 1;
        String trajectoryId = "trajectory-2";
        int limit = 10;

        List<GeolifeWaypoint> expected = waypoints.stream().filter(wp -> wp.getPersonId() == personId && wp.getTrajectoryId().equals(trajectoryId))
                .collect(Collectors.toList());
        Double cenLat = expected.stream().mapToDouble(GeolifeWaypoint::getLat).average().getAsDouble();
        Double cenLng = expected.stream().mapToDouble(GeolifeWaypoint::getLng).average().getAsDouble();

        Mono<ResponseEntity<List<GeolifeWaypoint>>> trajectory = app.findWaypointsForPersonAndTrajectory(personId, trajectoryId, limit);
        StepVerifier.create(trajectory).assertNext(t -> {
            List<GeolifeWaypoint> list = t.getBody();
            assertThat(list).containsAll(expected);
        }).verifyComplete();
    }

    private GeolifeWaypoint createWaypoint(int personId, String trajectoryId, LocalDateTime ts) {
        return GeolifeWaypoint.builder()
                .id(UUID.randomUUID().toString())
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(random.nextFloat())
                .lng(random.nextFloat())
                .altitude(random.nextInt())
                .timestamp(ts)
                .build();
    }
}