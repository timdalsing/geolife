package dev.dalsing.geolife.proximity;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.model.ProximityAlert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.io.WKTReader;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static java.time.temporal.ChronoUnit.MILLIS;

@ExtendWith(MockitoExtension.class)
class ProximityProcessorAppTest {

    ProximityProcessorApp app;

    @Mock
    JdbcTemplate jdbcTemplate;

    private static final GeometryFactory geometryFactory = new GeometryFactory();

    @BeforeEach
    void setUp() {
        app = new ProximityProcessorApp(jdbcTemplate);
    }

    @Test
    void process_success() throws Exception {
        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);
        String locationName = "Naval Air Station Jacksonville";
        String message = "Got a little too close to Naval Air Station Jacksonville";

        String wkt = "MULTIPOLYGON (((30 20, 45 40, 10 40, 30 20)), ((15 5, 40 10, 10 20, 5 10, 15 5)))";
        WKTReader wktReader = new WKTReader(geometryFactory);
        MultiPolygon boundaries = (MultiPolygon) wktReader.read(wkt);


        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        ProximityAlert expected = ProximityAlert.builder()
                .waypoint(waypoint)
                .locationName(locationName)
                .message(message)
                .boundaries(boundaries)
                .build();

        String[] queryResult = {"Naval Air Station Jacksonville", wkt};
        List<String[]> queryResults = new ArrayList<>();
        queryResults.add(queryResult);

        Mockito.when(jdbcTemplate.query(Mockito.anyString(), Mockito.any(RowMapper.class))).thenReturn(queryResults);

        Flux<GeolifeWaypoint> waypointFlux = Flux.just(waypoint);

        Function<Flux<GeolifeWaypoint>, Flux<ProximityAlert>> function = app.process();

        Flux<ProximityAlert> alertFlux = function.apply(waypointFlux);

        StepVerifier.create(alertFlux).assertNext(actual -> Assertions.assertThat(actual).isEqualTo(expected)).verifyComplete();
    }
}