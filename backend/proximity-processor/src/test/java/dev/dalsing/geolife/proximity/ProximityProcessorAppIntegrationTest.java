package dev.dalsing.geolife.proximity;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.model.ProximityAlert;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;

import static java.time.temporal.ChronoUnit.MILLIS;

@SpringBootTest
class ProximityProcessorAppIntegrationTest {

    @Autowired
    ProximityProcessorApp app;

    @Tag("integration")
    @Test
    void process() {
        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory";
        float lat = 30.229368f;
        float lng = -81.685956f;
        int alt = 10;
        LocalDateTime ts = LocalDateTime.now().truncatedTo(MILLIS);

        String locationName = "Naval Air Station Jacksonville";
        String message = "Got a little too close to Naval Air Station Jacksonville";

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        Flux<GeolifeWaypoint> waypointFlux = Flux.just(waypoint);
        Flux<ProximityAlert> alertFlux = app.process().apply(waypointFlux);

        StepVerifier.create(alertFlux).assertNext(alert -> {
            Assertions.assertThat(alert.getWaypoint()).isEqualTo(waypoint);
            Assertions.assertThat(alert.getLocationName()).isEqualTo(locationName);
            Assertions.assertThat(alert.getMessage()).isEqualTo(message);
        }).verifyComplete();
    }
}