package dev.dalsing.geolife.proximity;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;

@Slf4j
public class LoadShapeFile {

    @Disabled
    @Test
    void load() throws Exception {
        GeometryFactory geometryFactory = new GeometryFactory();
        Point point = geometryFactory.createPoint(new Coordinate(0.0, 0.0));
        Geometry buffer = point.buffer(1.0);
        log.info("{}", buffer);
//        CoordinateReferenceSystem crs = CRS.decode("EPSG:16005");
//        log.info("{}", crs);
//        Set<String> epsg = CRS.getSupportedCodes("EPSG");
//        log.info("{}", epsg);
//        Set<String> supportedAuthorities = CRS.getSupportedAuthorities(false);
//        log.info("{}", supportedAuthorities);
//        Map<String, Object> params = Map.of("url",
//                new File("/Users/tdalsing/workspace/projects/geolife/misc/data/utm/utmzone.shp").toURI().toURL());
//
//        DataStore dataStore = DataStoreFinder.getDataStore(params);
//        String typeName = dataStore.getTypeNames()[0];
//        SimpleFeatureSource featureSource = dataStore.getFeatureSource(typeName);
//        IncludeFilter include = Filter.INCLUDE;
//        SimpleFeatureCollection features = featureSource.getFeatures(include);
//        SimpleFeatureIterator iterator = features.features();
//        while (iterator.hasNext()) {
//            SimpleFeature feature = iterator.next();
//            SimpleFeatureType featureType = feature.getFeatureType();
//            ReferenceIdentifier crs = featureType.getCoordinateReferenceSystem().getCoordinateSystem().getName();
//            List<Object> attributes = feature.getAttributes();
//            log.info("feature: id = {}, type = {}, name = {}, attrs[3] = {}, attrs[4] = {}, attrs[5] = {}, featureType = {}",
//                    feature.getID(), featureType.getTypeName(), feature.getName().getLocalPart(),
//                    attributes.get(3), attributes.get(4), attributes.get(5), featureType);
//        }
    }

    /*

EPSG:16005 - UTM zone 5N
EPSG:16006 - UTM zone 6N
EPSG:16007 - UTM zone 7N
EPSG:16008 - UTM zone 8N
EPSG:16009 - UTM zone 9N
EPSG:16054 - UTM zone 54N
EPSG:16055 - UTM zone 55N
EPSG:16056 - UTM zone 56N
EPSG:16057 - UTM zone 57N
EPSG:16058 - UTM zone 58N
EPSG:16059 - UTM zone 59N
EPSG:16060 - UTM zone 60N
EPSG:16105 - UTM zone 5S
EPSG:16106 - UTM zone 6S
EPSG:16107 - UTM zone 7S
EPSG:16108 - UTM zone 8S
EPSG:16109 - UTM zone 9S
EPSG:16154 - UTM zone 54S
EPSG:16155 - UTM zone 55S
EPSG:16156 - UTM zone 56S
EPSG:16157 - UTM zone 57S
EPSG:16158 - UTM zone 58S
EPSG:16159 - UTM zone 59S
EPSG:16160 - UTM zone 60S
EPSG:16001 - UTM zone 1N
EPSG:16002 - UTM zone 2N
EPSG:16003 - UTM zone 3N
EPSG:16004 - UTM zone 4N
EPSG:16010 - UTM zone 10N
EPSG:16011 - UTM zone 11N
EPSG:16012 - UTM zone 12N
EPSG:16013 - UTM zone 13N
EPSG:16014 - UTM zone 14N
EPSG:16015 - UTM zone 15N
EPSG:16016 - UTM zone 16N
EPSG:16017 - UTM zone 17N
EPSG:16018 - UTM zone 18N
EPSG:16019 - UTM zone 19N
EPSG:16020 - UTM zone 20N
EPSG:16021 - UTM zone 21N
EPSG:16022 - UTM zone 22N
EPSG:16023 - UTM zone 23N
EPSG:16024 - UTM zone 24N
EPSG:16025 - UTM zone 25N
EPSG:16026 - UTM zone 26N
EPSG:16027 - UTM zone 27N
EPSG:16028 - UTM zone 28N
EPSG:16029 - UTM zone 29N
EPSG:16030 - UTM zone 30N
EPSG:16031 - UTM zone 31N
EPSG:16032 - UTM zone 32N
EPSG:16033 - UTM zone 33N
EPSG:16034 - UTM zone 34N
EPSG:16035 - UTM zone 35N
EPSG:16036 - UTM zone 36N
EPSG:16037 - UTM zone 37N
EPSG:16038 - UTM zone 38N
EPSG:16039 - UTM zone 39N
EPSG:16040 - UTM zone 40N
EPSG:16041 - UTM zone 41N
EPSG:16042 - UTM zone 42N
EPSG:16043 - UTM zone 43N
EPSG:16044 - UTM zone 44N
EPSG:16045 - UTM zone 45N
EPSG:16046 - UTM zone 46N
EPSG:16047 - UTM zone 47N
EPSG:16048 - UTM zone 48N
EPSG:16049 - UTM zone 49N
EPSG:16050 - UTM zone 50N
EPSG:16051 - UTM zone 51N
EPSG:16052 - UTM zone 52N
EPSG:16053 - UTM zone 53N
EPSG:16101 - UTM zone 1S
EPSG:16102 - UTM zone 2S
EPSG:16103 - UTM zone 3S
EPSG:16104 - UTM zone 4S
EPSG:16110 - UTM zone 10S
EPSG:16111 - UTM zone 11S
EPSG:16112 - UTM zone 12S
EPSG:16113 - UTM zone 13S
EPSG:16114 - UTM zone 14S
EPSG:16115 - UTM zone 15S
EPSG:16116 - UTM zone 16S
EPSG:16117 - UTM zone 17S
EPSG:16118 - UTM zone 18S
EPSG:16119 - UTM zone 19S
EPSG:16120 - UTM zone 20S
EPSG:16121 - UTM zone 21S
EPSG:16122 - UTM zone 22S
EPSG:16123 - UTM zone 23S
EPSG:16124 - UTM zone 24S
EPSG:16125 - UTM zone 25S
EPSG:16126 - UTM zone 26S
EPSG:16127 - UTM zone 27S
EPSG:16128 - UTM zone 28S
EPSG:16129 - UTM zone 29S
EPSG:16130 - UTM zone 30S
EPSG:16131 - UTM zone 31S
EPSG:16132 - UTM zone 32S
EPSG:16133 - UTM zone 33S
EPSG:16134 - UTM zone 34S
EPSG:16135 - UTM zone 35S
EPSG:16136 - UTM zone 36S
EPSG:16137 - UTM zone 37S
EPSG:16138 - UTM zone 38S
EPSG:16139 - UTM zone 39S
EPSG:16140 - UTM zone 40S
EPSG:16141 - UTM zone 41S
EPSG:16142 - UTM zone 42S
EPSG:16143 - UTM zone 43S
EPSG:16144 - UTM zone 44S
EPSG:16145 - UTM zone 45S
EPSG:16146 - UTM zone 46S
EPSG:16147 - UTM zone 47S
EPSG:16148 - UTM zone 48S
EPSG:16149 - UTM zone 49S
EPSG:16150 - UTM zone 50S
EPSG:16151 - UTM zone 51S
EPSG:16152 - UTM zone 52S
EPSG:16153 - UTM zone 53S
    */
}
