package dev.dalsing.geolife.proximity;

import dev.dalsing.geolife.model.GeolifeWaypoint;
import dev.dalsing.geolife.model.ProximityAlert;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.io.WKTReader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.function.Function;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class ProximityProcessorApp {

    JdbcTemplate jdbcTemplate;
    private static final GeometryFactory geometryFactory = new GeometryFactory();

    public static void main(String[] args) {
        SpringApplication.run(ProximityProcessorApp.class, args);
    }

    @Bean
    public Function<Flux<GeolifeWaypoint>, Flux<ProximityAlert>> process() {
        return flux -> flux.map(wp -> {
            try {
                return check(wp);
            } catch (Exception e) {
                throw new IllegalArgumentException(e.toString(),e);
            }
        });
    }

    private ProximityAlert check(GeolifeWaypoint waypoint) throws Exception {
        String sql = String.format("select fullname, ST_AsText(geom) from tl_2021_us_mil where ST_Intersects(geom, ST_Buffer(ST_GeomFromText('POINT(%f %f)'), 0.00001))",
                waypoint.getLng(), waypoint.getLat());
        List<String[]> results = jdbcTemplate
                .query(sql, (rs, rowNum) -> new String[]{rs.getString(1),rs.getString(2)});

        if (results.isEmpty()) {
            return null;
        } else {
            String[] result = results.get(0);

            String locationName = result[0];
            String wkt = result[1];
            WKTReader wktReader = new WKTReader(geometryFactory);
            MultiPolygon boundaries = (MultiPolygon) wktReader.read(wkt);

            return ProximityAlert.builder()
                    .waypoint(waypoint)
                    .locationName(locationName)
                    .message(String.format("Got a little too close to %s", locationName))
                    .boundaries(boundaries)
                    .build();
        }
    }
}
