create database geolife;
create table waypoint (
    id varchar(40) primary key,
    person_id int not null,
    trajectory_id varchar(40) not null,
    lat float not null,
    lng float not null,
    altitude int not null,
    ts timestamp not null
);
create index person_ndx on waypoint (person_id);
create index trajectory_ndx on waypoint (trajectory_id);