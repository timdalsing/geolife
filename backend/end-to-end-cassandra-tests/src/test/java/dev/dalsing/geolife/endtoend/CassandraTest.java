package dev.dalsing.geolife.endtoend;

import dev.dalsing.cassandra.common.CassandraWaypoint;
import dev.dalsing.geolife.pb.PBWaypoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.util.MimeType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static java.time.Duration.ofSeconds;
import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@SpringBootTest
class CassandraTest {

    @Autowired
    TestApp app;

    @Autowired
    StreamBridge bridge;

    @Autowired
    CassandraTemplate cassandraTemplate;

    @BeforeEach
    void setUp() {
        cassandraTemplate.truncate(CassandraWaypoint.class);
    }

    @AfterEach
    void tearDown() {
        cassandraTemplate.truncate(CassandraWaypoint.class);
    }

    @Tag("integration")
    @Test
    void cassandra_success() {
        int personId = 1;
        String trajectoryId = UUID.randomUUID().toString();
        float lat = 1.23f;
        float lng = 2.34f;
        int alt = 100;
        LocalDateTime now = LocalDateTime.now().truncatedTo(MILLIS);
        long ts = now.toInstant(UTC).toEpochMilli();

        PBWaypoint pbWaypoint = PBWaypoint.newBuilder()
                .setPersonId(personId)
                .setTrajectoryId(trajectoryId)
                .setLat(lat)
                .setLng(lng)
                .setAltitude(alt)
                .setTimestamp(ts)
                .build();

        bridge.send("pb-out-0", pbWaypoint, MimeType.valueOf("application/x-protobuf"));

        await()
                .pollDelay(ofSeconds(2L))
                .pollInterval(ofSeconds(2L))
                .atMost(ofSeconds(10L))
                .untilAsserted(() -> {
                    List<CassandraWaypoint> actualList = cassandraTemplate.query(CassandraWaypoint.class).all();
                    assertThat(actualList).hasSize(1);
                    CassandraWaypoint actualWaypoint = actualList.get(0);

                    assertThat(actualWaypoint.getKey().getPersonId()).isEqualTo(personId);
                    assertThat(actualWaypoint.getKey().getTrajectoryId()).isEqualTo(trajectoryId);
                    assertThat(actualWaypoint.getLat()).isEqualTo(lat);
                    assertThat(actualWaypoint.getLng()).isEqualTo(lng);
                    assertThat(actualWaypoint.getAltitude()).isEqualTo(alt);
                    assertThat(actualWaypoint.getTimestamp()).isEqualTo(now);
                });
    }
}