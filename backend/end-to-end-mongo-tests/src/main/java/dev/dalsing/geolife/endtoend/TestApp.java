package dev.dalsing.geolife.endtoend;

import dev.dalsing.geolife.pb.common.WaypointMessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({MongoConfig.class, WaypointMessageConverter.class})
public class TestApp {

    public static void main(String[] args) {
        SpringApplication.run(TestApp.class, args);
    }
}
