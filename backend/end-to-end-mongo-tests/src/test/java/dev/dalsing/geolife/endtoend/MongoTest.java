package dev.dalsing.geolife.endtoend;

import dev.dalsing.geolife.mongo.common.MongoWaypoint;
import dev.dalsing.geolife.pb.PBWaypoint;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.util.MimeType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static java.time.Duration.ofSeconds;
import static java.time.ZoneOffset.UTC;
import static java.time.temporal.ChronoUnit.MILLIS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

@SpringBootTest
public class MongoTest {

    @Autowired
    TestApp app;

    @Autowired
    StreamBridge bridge;

    @Autowired
    ReactiveMongoTemplate mongoTemplate;

    @BeforeEach
    void setUp() {
        mongoTemplate.remove(MongoWaypoint.class).all().subscribe();
    }

    @AfterEach
    void tearDown() {
        mongoTemplate.remove(MongoWaypoint.class).all().subscribe();
    }

    @Tag("integration")
    @Test
    void mongo_success() {
        int personId = 1;
        String trajectoryId = UUID.randomUUID().toString();
        float lat = 1.23f;
        float lng = 2.34f;
        int alt = 100;
        LocalDateTime now = LocalDateTime.now().truncatedTo(MILLIS);
        long ts = now.toInstant(UTC).toEpochMilli();

        PBWaypoint pbWaypoint = PBWaypoint.newBuilder()
                .setPersonId(personId)
                .setTrajectoryId(trajectoryId)
                .setLat(lat)
                .setLng(lng)
                .setAltitude(alt)
                .setTimestamp(ts)
                .build();

        bridge.send("pb-out-0", pbWaypoint, MimeType.valueOf("application/x-protobuf"));

        await()
                .pollDelay(ofSeconds(2L))
                .pollInterval(ofSeconds(2L))
                .atMost(ofSeconds(10L))
                .untilAsserted(() -> {
                    List<MongoWaypoint> actualList = mongoTemplate.query(MongoWaypoint.class).all().collectList().block();
                    assertThat(actualList).hasSize(1);
                    MongoWaypoint actual = actualList.get(0);
                    assertThat(actual.getPersonId()).isEqualTo(personId);
                    assertThat(actual.getTrajectoryId()).isEqualTo(trajectoryId);
                    assertThat(actual.getLat()).isEqualTo(lat);
                    assertThat(actual.getLng()).isEqualTo(lng);
                    assertThat(actual.getAltitude()).isEqualTo(alt);
                    assertThat(actual.getTimestamp()).isEqualTo(now);
                });

    }
}
