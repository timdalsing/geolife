package dev.dalsing.geolife.cassandra;

import dev.dalsing.cassandra.common.CassandraWaypoint;
import dev.dalsing.cassandra.common.CassandraWaypointKey;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.UUID;
import java.util.function.Consumer;

import static org.springframework.data.cassandra.core.query.Criteria.where;
import static org.springframework.data.cassandra.core.query.Query.query;

@SpringBootTest
class CassandraConsumerAppIntegrationTest {

    @Autowired
    CassandraConsumerApp app;

    @Autowired
    ReactiveCassandraTemplate template;

    static Random random = new Random();

    String id = UUID.randomUUID().toString();
    int personId = random.nextInt(1000);
    String trajectoryId = UUID.randomUUID().toString();
    float lat = random.nextFloat();
    float lng = random.nextFloat();
    int alt = random.nextInt(1000);
    LocalDateTime ts = LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS);

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Tag("integration")
    @Test
    void consumer() {
        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        CassandraWaypoint cassandraWaypoint = CassandraWaypoint.builder()
                .key(CassandraWaypointKey.builder()
                        .id(id)
                        .personId(personId)
                        .trajectoryId(trajectoryId)
                        .build())
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        CassandraWaypointByTrajectory trajectoryWaypoint = CassandraWaypointByTrajectory.builder()
                .key(CassandraWaypointByTrajectoryKey.builder()
                        .id(id)
                        .personId(personId)
                        .trajectoryId(trajectoryId)
                        .build())
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        Consumer<Flux<GeolifeWaypoint>> consumer = app.consumer();
        Flux<GeolifeWaypoint> waypointFlux = Flux.just(waypoint);
        consumer.accept(waypointFlux);

        Flux<CassandraWaypoint> cassandraWaypointFlux = template
                .select(query(where("key.id").is(id))
                        .and(where("key.personId").is(personId))
                        .and(where("key.trajectoryId").is(trajectoryId)), CassandraWaypoint.class);
        StepVerifier.create(cassandraWaypointFlux).assertNext(wp -> Assertions.assertThat(wp).isEqualTo(cassandraWaypoint)).verifyComplete();

        Flux<CassandraWaypointByTrajectory> trajectoryFlux = template
                .select(query(where("key.id").is(id))
                        .and(where("key.personId").is(personId))
                        .and(where("key.trajectoryId").is(trajectoryId)), CassandraWaypointByTrajectory.class);
        StepVerifier.create(trajectoryFlux).assertNext(wp -> Assertions.assertThat(wp).isEqualTo(trajectoryWaypoint)).verifyComplete();
    }
}