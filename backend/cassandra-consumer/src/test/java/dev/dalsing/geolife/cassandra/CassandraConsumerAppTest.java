package dev.dalsing.geolife.cassandra;

import dev.dalsing.cassandra.common.CassandraWaypoint;
import dev.dalsing.cassandra.common.CassandraWaypointKey;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import io.micrometer.core.instrument.Timer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.cassandra.core.ReactiveCassandraBatchOperations;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import org.springframework.data.cassandra.core.WriteResult;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CassandraConsumerAppTest {

    CassandraConsumerApp app;

    @Mock
    ReactiveCassandraTemplate template;

    @Mock
    Timer timer;

    @BeforeEach
    void setUp() {
        app = new CassandraConsumerApp(template, timer);
    }

    @Test
    void consumer_success() {
        Consumer<Flux<GeolifeWaypoint>> consumer = app.consumer();

        String id = "some-id";
        int personId = 1;
        String trajectoryId = "some-trajectory-id";
        float lat = 12.34f;
        float lng = 123.34f;
        int alt = 100;
        LocalDateTime ts = LocalDateTime.now();

        GeolifeWaypoint waypoint = GeolifeWaypoint.builder()
                .id(id)
                .personId(personId)
                .trajectoryId(trajectoryId)
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        Flux<GeolifeWaypoint> waypointFlux = Flux.just(waypoint);

        ReactiveCassandraBatchOperations batchOps = Mockito.mock(ReactiveCassandraBatchOperations.class);
        WriteResult writeResult = Mockito.mock(WriteResult.class);
        when(template.batchOps()).thenReturn(batchOps);
        when(batchOps.insert(any(List.class))).thenReturn(batchOps);
        when(batchOps.execute()).thenReturn(Mono.just(writeResult));

        Mockito.doAnswer(invocation -> {
            Supplier runnable = invocation.getArgument(0);
            return runnable.get();
        }).when(timer).record(any(Supplier.class));

        consumer.accept(waypointFlux);

        CassandraWaypoint cassandraWaypoint = CassandraWaypoint.builder()
                .key(CassandraWaypointKey.builder()
                        .id(id)
                        .personId(personId)
                        .trajectoryId(trajectoryId)
                        .build())
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();
        CassandraWaypointByTrajectory trajectoryWaypoint = CassandraWaypointByTrajectory.builder()
                .key(CassandraWaypointByTrajectoryKey.builder()
                        .id(id)
                        .personId(personId)
                        .trajectoryId(trajectoryId)
                        .build())
                .lat(lat)
                .lng(lng)
                .altitude(alt)
                .timestamp(ts)
                .build();

        verify(batchOps).insert(List.of(cassandraWaypoint, trajectoryWaypoint));
    }
}