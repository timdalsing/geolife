package dev.dalsing.geolife.cassandra;

import dev.dalsing.cassandra.common.CassandraPersonTrajectory;
import dev.dalsing.cassandra.common.CassandraPersonTrajectoryKey;
import dev.dalsing.cassandra.common.CassandraWaypoint;
import dev.dalsing.cassandra.common.CassandraWaypointKey;
import dev.dalsing.geolife.common.ObjectMapperConfig;
import dev.dalsing.geolife.model.GeolifeWaypoint;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.cassandra.core.ReactiveCassandraBatchOperations;
import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.function.Consumer;

import static lombok.AccessLevel.PRIVATE;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
@Import({ObjectMapperConfig.class})
@RequiredArgsConstructor
@Slf4j
@FieldDefaults(level = PRIVATE, makeFinal = true)
public class CassandraConsumerApp {

    ReactiveCassandraTemplate template;
    Timer timer;
    AppProperties properties;

    public static void main(String[] args) {
        SpringApplication.run(CassandraConsumerApp.class, args);
    }


    @Bean
    public static Timer timer(MeterRegistry registry) {
        return registry.timer("cassandra-consumer-timer");
    }

    @Bean
    public Consumer<Flux<GeolifeWaypoint>> consumer() {
        return flux -> {
            if (properties.getPrefetchRate() > 0) {
                // limit rate for single node cluster for demos
                flux = flux.limitRate(properties.getPrefetchRate());
            }

            flux
                    .bufferTimeout(properties.getMaxBufferSize(), Duration.ofMillis(properties.getBufferTimeout()))
                    .subscribe(list -> {
                        ReactiveCassandraBatchOperations batchOps = template.batchOps();

                        list.forEach(waypoint -> {
                            CassandraWaypoint cassandraWaypoint = CassandraWaypoint.builder()
                                    .key(CassandraWaypointKey.builder()
                                            .id(waypoint.getId())
                                            .personId(waypoint.getPersonId())
                                            .trajectoryId(waypoint.getTrajectoryId())
                                            .build())
                                    .lat(waypoint.getLat())
                                    .lng(waypoint.getLng())
                                    .altitude(waypoint.getAltitude())
                                    .timestamp(waypoint.getTimestamp())
                                    .build();

                            CassandraPersonTrajectory personTrajectory = CassandraPersonTrajectory.builder()
                                    .key(CassandraPersonTrajectoryKey.builder()
                                            .personId(waypoint.getPersonId())
                                            .trajectoryId(waypoint.getTrajectoryId())
                                            .build())
                                    .build();

                            batchOps.insert(cassandraWaypoint, personTrajectory);
                        });

                        timer.record(() -> batchOps.execute().subscribe());
                    });
        };
    }
}
