package dev.dalsing.geolife.cassandra;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static lombok.AccessLevel.PRIVATE;

@ConfigurationProperties("app")
@Data
@NoArgsConstructor
@FieldDefaults(level = PRIVATE)
public class AppProperties {

    int prefetchRate = 100;
    int maxBufferSize = 10;
    long bufferTimeout = 1000L;
}
