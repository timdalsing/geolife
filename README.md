# Geolife Spring Boot Demo

The purpose of this repo is to demonstrate the use of [Spring](https://spring.io/) techologies to ingest geospatial data from the 
[Geolife](https://www.microsoft.com/en-us/research/project/geolife-building-social-networks-using-human-location-history/)
project into various data stores and display the data on a map using a [Flutter](https://flutter.dev/) App.  Geolife is a project to capture
and share travel location histories using GPS trajectories, which was started in 2007.  The dataset includes trajectories
from 183 individuals.  The data is provided in CSV format, one waypoint per line.  The data can be freely downloaded and contains 1.7 GB of data.

The concept is to simulate an IoT system where the data is sent from a mobile device, such as a mobile phone or other
GPS receiver, to a data center for processing.  Each waypoint is captured as a compact message and sent individually.
This system processes the waypoint in a number of ways, including storing in PostgreSQL, Cassandra, and MongoDB, 
analyzing the data using ML clustering algorithms, and sending out notifications for analysis results.

This project does not include the tooling and infrastructure to actually capture the data on the devices and send it
to the system.  An emulator is used in its place.

Including 3 different data store types is simply to demonstrate how each would be used for this use case.  Only one is
probably needed in a real system. 

# Architecture

![Architecture](arch.jpg)

The driver is the emulator that simulates the mobile device and the infrastructure to capture and send the data.  It reads
the CSV files and creates one message per line from the files.  It is rate limited to 1000/second.  The message is in
[Protobuf](https://github.com/protocolbuffers/protobuf) format, which is an efficient and small binary serialization 
technology widely used in IoT systems due to the small size of the serialized message.  The Protobuf message is sent
to the `waypoint-pb` topic in [Kafka](https://kafka.apache.org/).

The Protobuf message is consumed by the `pb-to-json-service` component, which receives the message, converts it to
JSON format, and sends it to the `waypoint` topic.

Each consumer of the `waypoint` topic uses different [Kafka consumer groups](https://www.educba.com/kafka-consumer-group/), 
so they each receive the same message.  For the JDBC Consumer, Cassandra Consumer, and MongoDB Consumer, they simply
store the waypoint in a data store.

The Density Processor component receives the waypoint and performs [cluster analysis](https://byjus.com/maths/cluster-analysis/)
on the latitude/longitude combination using the [KMeans](https://towardsmachinelearning.org/k-means/) algorithm.  The
analysis uses [Statistical Machine Intelligence and Learning Engine (SMILE)](https://haifengl.github.io/) Java Machine Learning
library.  The waypoint is assigned to a cluster and a density analysis message is generated, which is sent to the 
`density-result` topic.

The Density Analysis component receives the message from the `density-result` topic and updates a count in a PostgreSQL
table for counts for each cluster.  This information is used to calculate percentages of waypoints in each cluster.
A Density Analysis message is generated for the updated percentage for the cluster, which is sent to the 
`density-analysis` topic.

Future work includes creating a component that receives the Density Analysis message and sending a message to the mobile
app or web site using [Websockets](https://www.educba.com/what-is-websocket/).

The Mobile App can connect to any of the 3 data store services, since they provide the same API.  The app provides a list
of trajectories from which the user can select from.  It then displays a Google Map that shows the waypoints for the 
selected trajectory.

# Technologies Used

- [Java](https://www.oracle.com/java/)
- [Spring Boot](https://spring.io/projects/spring-boot)
- [Spring Webflux](https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html) - a reactive version of Spring Web.
- [Spring Cloud Stream](https://spring.io/projects/spring-cloud-stream) - technology to consume and produce message streams using message brokers, such as Kafka, RabbitMQ, Kinesis, and others.
- [Spring Cloud Stream Kafka Binder](https://github.com/spring-cloud/spring-cloud-stream-binder-kafka) - the binder for Spring Cloud Stream that works with Kafka
- [Spring Data JDBC](https://spring.io/projects/spring-data-jdbc) - simple technology to access any database that provides a JDBC driver using the JdbcTemplate.
- [Spring Data Cassandra](https://spring.io/projects/spring-data-cassandra) - Spring's encapsulation of the Cassandra Java APIs.
- [Spring Data MongoDB](https://spring.io/projects/spring-data-mongodb) - Spring's encapsulation of the MongoDB Java APIs.
- [Kafka](https://kafka.apache.org/)
- [Spring Actuator](https://docs.spring.io/spring-boot/docs/current/reference/html/actuator.html#actuator) - a Spring Boot add-on that captures metrics for the running app, includes JVM metrics.  Nearly all Spring modules support Actuator, including Web, Webflux, and most Spring Data modules.
- [Micrometer](https://micrometer.io/) - metrics capture library used by Spring Actuator.  Custom metrics can be created using Micrometer.
- [Micrometer Graphite Registry](https://micrometer.io/docs/registry/graphite) - registry plugin for Micrometer that writes metrics to Graphite.
- [Graphite](https://graphiteapp.org/) - a metrics capture and display tool.
- [Spring Cloud Sleuth](https://spring.io/projects/spring-cloud-sleuth) - Spring plugin for Web, Webflux, and Spring Cloud Stream that traces calls across HTTP and messaging boundaries.  Used to help debug complex interactions between components.
- [Spring Cloud Sleuth Zipkin](https://github.com/spring-cloud/spring-cloud-sleuth/tree/3.1.x/spring-cloud-sleuth-zipkin) - Sleuth plugin for Zipkin
- [Zipkin](https://zipkin.io/) - tool that captures and displays traces from Spring Cloud Sleuth
- [Project Reactor](https://projectreactor.io/) - reactive library written in Java by the Spring team
- [Protobuf](https://github.com/protocolbuffers/protobuf) - serialization technology created by Google, with binders for many languages.  Widely used for IoT.
- [PostgreSQL](https://www.postgresql.org/) - SQL database
- [PostGIS](http://www.postgis.net/) - extensions to PostgreSQL for geospatial data storage and processing
- [Cassandra](https://cassandra.apache.org) - NoSQL database that can handle very high data rates.  Often used for IoT due to it's high performance.
- [MongoDB](https://www.mongodb.com/) - document database
- [SMILE](https://haifengl.github.io/) - machine learning library written in Java
- [Flutter](https://flutter.dev/) - technology created by Google for building web and mobile apps using a single code base, reminiscent of React and React Native.  Based on [Dart](https://dart.dev/) programming language.
- [Google Maps Platform](https://developers.google.com/maps/apis-by-platform) - APIs and technologies for embedding Google Maps in web and mobile apps.

# Components

## `pb-to-json-service`

This component is a Spring Cloud Stream app that receives a message in Protobuf format and converts it to JSON using a POJO.
All POJOs are in the `model` component.  The consuming topic is `waypoint-pb` and the producing topic is `waypoint`.

## `jdbc-consumer`

This component receives the message on the `waypoint` topic and stores the data in the `waypoint` table in PostgreSQL.
[Spring Data JDBC](https://spring.io/projects/spring-data-jdbc) is used to store the waypoint.

## `cassandra-consumer`

This component receives the message on the `waypoint` topic and stores the data in the `waypoint` and `waypoint_trajectory` tables in Cassandra.
[Spring Data Cassandra](https://spring.io/projects/spring-data-cassandra) is used, including the reactive extensions.
The waypoint POJO is directly stored in Cassandra using the mapping feature in Spring Data Cassandra.

## `mongo-consumer`

This component receives the message on the `waypoint` topic and stores the data in the `waypoint` collection in MongoDB.
[Spring Data MongoDB](https://spring.io/projects/spring-data-mongodb) is used, including the reactive extensions.
The waypoint POJO is directly stored in MongoDB using the mapping feature in Spring Data MongoDB.

## `density-processor`

This component uses Spring Cloud Stream to process the message from the `waypoint` topic and produce a Density Result
message on the `density-result` topic.  It uses SMILE to calculate the cluster for the longitude/latitude combination.  This 
results in a clustering analysis that can suggest a correlation with certain points of interest if the waypoints
are more closely associated with certain areas, which is how the cluster can be interpreted.  KMeans is used to do
the clustering analysis.  A sample of the data is used to train the KMeans algorithm.

## `density-consumer`

This component uses Spring Cloud Stream to consume the Density Result message from the `density-result` topic and store
it in PostgreSQL.  It uses Spring Data JDBC to store the data.

## `density-analysis`

This component uses Spring Cloud Stream to analyze the message from the `density-result` topic and calculate percentages
for each cluster.  It updates a count for the cluster in PostgreSQL using Spring Data JDBC.  The percentages are then 
calculated for each cluster, and a Density Analysis message is generated for the updated cluster and sent to the 
`density-analysis` topic.  To avoid concurrency issues, [Kafka Paritioning](https://medium.com/event-driven-utopia/understanding-kafka-topic-partitions-ae40f80552e8) 
is used to ensure only a single consumer instance processes a particular cluster, which is [supported by Spring Cloud Stream](https://cloud.spring.io/spring-cloud-stream-binder-kafka/spring-cloud-stream-binder-kafka.html#_partitioning_with_the_kafka_binder).

## `jdbc-service`

This component uses Spring Webflux to provide a REST/JSON endpoint to access waypoint data stored in PostgreSQL.

## `cassandra-service`

This component uses Spring Webflux to provide a REST/JSON endpoint to access waypoint data stored in Cassandra.

## `mongo-service`

This component uses Spring Webflux to provide a REST/JSON endpoint to access waypoint data stored in MongoDB.

## `density-service`

This component uses Spring Webflux to provide a REST/JSON endpoint to access density result data stored in PostgreSQL.

## `cassandra-common`

Common classes for Cassandra, including POJOs and Spring Data repositories for accessing the data.

## `mongo-common`

Common classes for MongoDB, including POJOs for accessing the data.

## `pb` and `pb-common`

The `pb` component includes the Protobuf `.proto` file and the generated Java sources file produced by `protoc`.  The
`pb-common` component provides the custom `MessageConverter` implementation required by Spring Cloud Stream to serialize
and deserialize the Protobuf data.

## `model`

This component contains the shared POJO data model classes used by most other components.

## `common`

This component contains various shared classes.  Currently it includes a customized implementation of the 
[Jackson ObjectMapper](https://www.baeldung.com/jackson-object-mapper-tutorial) that includes `java.time` support 
required for `LocalDateTime` conversion in JSON that is not automatically provided by Spring Cloud Stream.

## `alert-notif`

This component uses Spring to expose a Websockets interface to receive Density Analysis alerts.  To be used by the mobile
app in the future, but currently not being used.

## `proximity-processor`

This component uses PostgreSQL and [PostGIS](http://www.postgis.net/) to determine whether a waypoint is close to a military installation.  The 
geospatial data for boundaries of military installations was obtained from the [Census Bureau](https://www.census.gov/geographies/mapping-files/time-series/geo/tiger-line-file.html).
This component is not currently being used.

# Future Plans

- Mobile App for Capturing Data.  A new mobile app that will capture trajectory data and send waypoints via MQTT to the backends
- Kubernetes. Deployment descriptors and other artifacts that will be used to automatically deploy the components in this project
to Kubernetes.
- Jib. [Jib](https://github.com/GoogleContainerTools/jib) will be used to generate Docker images of each deployable component.
- Spring Security Oauth2.  Secure the mobile app and REST/JSON endpoints using Oauth and JWT.
- Add Websockets to Existing Mobile App
- MQTT.  Add MQTT support for new Mobile App.
- Add web app for receiving notifications.