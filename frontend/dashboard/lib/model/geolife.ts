interface Waypoint {
    id: string
    personId: number
    trajectoryId: string
    lat: number
    lng: number
    altitude: number
    timestamp: string
}

interface DensityResult {
    waypoint: Waypoint
    cluster: number
    centroid: number[]
}

interface DensityAlert {
    result: DensityResult
    count: number
    percentage: number
}