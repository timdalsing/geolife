import { action, makeAutoObservable, observable } from "mobx"

export class PersonId {

    personId: number = -1

    constructor() {
        this.personId = -1
        makeAutoObservable(this)
    }

    setPersonId(personId: number): number {
        this.personId = personId
        return this.personId
    }
}

export const personIdStore = new PersonId()
