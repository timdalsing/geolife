import { action, makeAutoObservable, observable } from "mobx"

export class TrajectoryId {

    trajectoryId: string | null = null

    constructor() {
        this.trajectoryId = null
        makeAutoObservable(this)
    }

    setTrajectoryId(trajectoryId: string | null): string | null {
        this.trajectoryId = trajectoryId
        return this.trajectoryId
    }
}

export const trajectoryIdStore = new TrajectoryId()
