import { Client } from '@stomp/stompjs'

export const client = new Client({
    brokerURL: 'ws://localhost:8631/notif',
    debug: function (str) {
        console.log(`stomp debug: ${str}`);
    },
    reconnectDelay: 5000,
    heartbeatIncoming: 4000,
    heartbeatOutgoing: 4000,
    onStompError(frame) {
        console.log(`Stomp error: ${frame.headers['message']}`)
        console.log(`Stomp error details: ${frame.body}`)
    },
})
