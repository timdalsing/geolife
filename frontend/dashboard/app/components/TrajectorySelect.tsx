'use client'

import { PersonId } from "@/lib/store/personIdStore"
import { trajectoryIdStore } from "@/lib/store/trajectoryIdStore"
import { observer } from "mobx-react"
import { ChangeEvent, useEffect, useState } from "react"

interface TrajectorySelectProps {
    personId: PersonId
}

const TrajectorySelect = observer((props: TrajectorySelectProps) => {

    const personId = props.personId.personId
    const [trajectoryId, setTrajectoryId] = useState<string>("")
    const [trajectoryIds, setTrajectoryIds] = useState<string[]>([])

    useEffect(() => {
        trajectoryIdStore.setTrajectoryId(null)
        setTrajectoryId("")
        setTrajectoryIds([])
        if (personId >= 0) {
            fetch(`http://localhost:8615/trajectory/${personId}`)
                .then((resp) => resp.json())
                .then((data) => setTrajectoryIds(data))
                .catch((err) => console.log(`TrajectorySelect.useEffect: error = ${err}`))
        }
    }, [personId])

    function changeTrajectoryId(event: ChangeEvent) {
        const target: HTMLInputElement = event.target as HTMLInputElement
        const value = target.value as string
        setTrajectoryId(value)
        trajectoryIdStore.setTrajectoryId(value == "" ? null : value)
    }

    return (
        <div className="flex flex-row my-3">
            <div className="mr-4 ml-4">Trajectory ID:</div>
            <select className="text-black" onChange={changeTrajectoryId} defaultValue={trajectoryId}>
                <option value={""}>Select a Trajectory ID</option>
                {trajectoryIds.map(id => <option key={id} value={id}>{id}</option>)}
            </select>
        </div>
    )
})

export default TrajectorySelect