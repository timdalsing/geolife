'use client'

import { personIdStore } from "@/lib/store/personIdStore"
import { trajectoryIdStore } from "@/lib/store/trajectoryIdStore"
import { ChangeEvent, useEffect, useState } from "react"

export default function PersonSelect() {

    const [personIds, setPersonIds] = useState<number[]>([])

    useEffect(() => {
        fetch('http://localhost:8615/personIds')
            .then((resp) => resp.json())
            .then((data) => setPersonIds(data))
            .catch((err) => console.log(`PersonSelect.useEffect: error = ${err}`))
    }, [])

    function changePersonId(event: ChangeEvent) {
        const target: HTMLInputElement = event.target as HTMLInputElement
        const id = Number(target.value)
        personIdStore.setPersonId(id)
        trajectoryIdStore.setTrajectoryId(null)
    }

    return (
        <div id='person-panel' className="flex flex-row my-3">
        <div id='person-panel-title' className="mr-4">Person ID:</div>
            <select className="text-black" onChange={changePersonId} defaultValue={-1}>
                <option value={-1}>Select a person ID</option>
                {personIds.map(id => <option key={id} value={id}>{id}</option>)}
            </select>
        </div>
    )
}
