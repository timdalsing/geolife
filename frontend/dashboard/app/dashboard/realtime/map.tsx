'use client'

import React, { useEffect, useMemo, useState } from "react";
import { useLoadScript, GoogleMap, Marker } from '@react-google-maps/api'
import { client } from "@/lib/ws/waypoints";

interface MapProps {
    personId: number
}

interface MapData {
    alerts: DensityAlert[]
    mapCenter: google.maps.LatLngLiteral
    zoom: number
}

export default function MapContainer(props: MapProps) {

    const [mapData, setMapData] = useState<MapData>({ alerts: [], mapCenter: { lat: 38.8612752, lng: -95.509252 }, zoom: 4 })

    const personId = props.personId

    const libraries = useMemo(() => ['places'], []);

    function calcCenter(alerts: DensityAlert[]): google.maps.LatLngLiteral {
        const lats = alerts.map(alert => alert.result.waypoint.lat)
        const lngs = alerts.map(alert => alert.result.waypoint.lng)
        const minLat = Math.min(...lats)
        const minLng = Math.min(...lngs)
        const maxLat = Math.max(...lats)
        const maxLng = Math.max(...lngs)

        const centerLat = (maxLat + minLat) / 2.0
        const centerLng = (maxLng + minLng) / 2.0

        return { lat: centerLat, lng: centerLng }
    }

    function calcZoom(alerts: DensityAlert[]): number {
        return 5 // TODO
    }

    function configMap(alerts: DensityAlert[]) {
        const center = calcCenter(alerts)
        const zoom = calcZoom(alerts)
        setMapData({ alerts: alerts, mapCenter: center, zoom: zoom })
    }

    useEffect(() => {
        setMapData({ alerts: [], mapCenter: { lat: 38.8612752, lng: -95.509252 }, zoom: 4 })

        if (personId >= 0) {
            client.onConnect = (frame) => {
                console.log(`MapContainer.useEffect.onConnect: ${frame.headers}`)
                client.subscribe(`/queue/${personId}`, (message) => {
                    const body = message.body
                    console.log(`MapContainer.onMessage: body = ${body}`)
                    const alert: DensityAlert = JSON.parse(body)
                    const alerts = mapData.alerts
                    configMap([...alerts, alert])
                })
            }
            console.log(`MapContainer.useEffect: client activate`)
            client.activate()
        }

        return () => {
            console.log(`MapContainer.useEffect: client deactivate`)
            client.deactivate()
        }
    }, [personId])

    const mapOptions = useMemo<google.maps.MapOptions>(
        () => ({
            disableDefaultUI: true,
            clickableIcons: true,
            scrollwheel: true,
            scaleControl: true,
            zoomControl: true,
        }),
        []
    );

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_API_KEY as string,
        libraries: libraries as any,
    });

    if (!isLoaded) {
        return <p>Loading...</p>;
    }

    function onMarkerLoad(marker: Marker) {
        console.log(`marker = ${marker}`)
    }

    function title(alert: DensityAlert): string {
        const count = alert.count
        const percent = alert.percentage

        const result: DensityResult = alert.result
        const centroid = result.centroid
        const cluster = result.cluster
        const waypoint = result.waypoint

        return `cluster = ${cluster} count = ${count} percent = ${percent} trajectory = ${waypoint.trajectoryId} timestamp = ${waypoint.timestamp}`
    }

    if (personId < 0) {
        return (<div>Select a person ID</div>)
    } else {
        return (
            <div>
                <GoogleMap
                    options={mapOptions}
                    zoom={mapData.zoom}
                    center={mapData.mapCenter}
                    mapTypeId={google.maps.MapTypeId.ROADMAP}
                    mapContainerStyle={{ width: '100em', height: '100em' }}
                    onLoad={() => console.log('Map Component Loaded...')}

                >
                    {mapData.alerts.map((alert) => <Marker
                        key={alert.result.waypoint.id}
                        position={{ lat: alert.result.waypoint.lat, lng: alert.result.waypoint.lng }}
                        clickable={true}
                        title={title(alert)} />)}
                </GoogleMap>
            </div>
        )
    }
}
