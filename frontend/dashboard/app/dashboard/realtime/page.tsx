'use client'

import { ChangeEvent, useEffect, useState } from "react";
import MapContainer from "./map";

export default function Realtime() {

    const [personId, setPersonId] = useState<number>(-1)
    const [personIds, setPersonIds] = useState<number[]>([])

    useEffect(() => {
        fetch('http://localhost:8615/personIds')
            .then((resp) => resp.json())
            .then((data) => setPersonIds(data))
            .catch((err) => console.log(`Person.useEffect: error = ${JSON.stringify(err)}`))
    }, [])


    function changePersonId(event: ChangeEvent) {
        const target: HTMLInputElement = event.target as HTMLInputElement
        const value = Number(target.value)
        setPersonId(value)
    }

    return (
        <div className="m-3">
            <div className="text-xl">Real-time</div>
            <div className="flex flex-row my-3">
                <div className="mr-4">Person ID:</div>
                <select className="text-black" onChange={changePersonId} defaultValue={-1}>
                    <option value={-1}>Select a person ID</option>
                    {personIds.map(id => <option key={id} value={id}>{id}</option>)}
                </select>
            </div>
            <div><MapContainer personId={personId} /></div>
        </div>
    )
}