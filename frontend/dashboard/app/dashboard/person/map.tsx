'use client'

import React, { useEffect, useMemo, useState } from "react";
import { useLoadScript, GoogleMap, Marker } from '@react-google-maps/api'
import { PersonId } from "@/lib/store/personIdStore";
import { observer } from "mobx-react";
import { TrajectoryId } from "@/lib/store/trajectoryIdStore";

interface MapData {
    waypoints: Waypoint[]
    mapCenter: google.maps.LatLngLiteral
    zoom: number
}

interface MapContainerProps {
    personId: PersonId
    trajectoryId: TrajectoryId
}

const MapContainer = observer((props: MapContainerProps) => {

    const personId = props.personId.personId
    const trajectoryId = props.trajectoryId.trajectoryId

    const [mapData, setMapData] = useState<MapData>({ waypoints: [], mapCenter: { lat: 38.8612752, lng: -95.509252 }, zoom: 4 })
    const libraries = useMemo(() => ['places'], []);

    function calcCenter(waypoints: Waypoint[]): google.maps.LatLngLiteral {
        const lats = waypoints.map(wp => wp.lat)
        const lngs = waypoints.map(wp => wp.lng)
        const minLat = Math.min(...lats)
        const minLng = Math.min(...lngs)
        const maxLat = Math.max(...lats)
        const maxLng = Math.max(...lngs)

        const centerLat = (maxLat + minLat) / 2.0
        const centerLng = (maxLng + minLng) / 2.0

        return { lat: centerLat, lng: centerLng }
    }

    function calcZoom(waypoints: Waypoint[]): number {
        return 15 // TODO
    }

    function configMap(waypoints: Waypoint[]) {
        const center = calcCenter(waypoints)
        const zoom = calcZoom(waypoints)
        setMapData({ waypoints: waypoints, mapCenter: center, zoom: zoom })
    }

    useEffect(() => {
        setMapData({ waypoints: [], mapCenter: { lat: 38.8612752, lng: -95.509252 }, zoom: 15 })

        if (personId >= 0 && trajectoryId != null) {
            console.log(`MapContainer.useEffect: personId = ${personId}, trajectoryId = ${trajectoryId}`)
            fetch(`http://localhost:8615/waypoint/${personId}/${trajectoryId}`)
                .then((res) => res.json())
                .then(data => configMap(data))
                .catch((err) => console.log(`MapContainer.useEffect: error = ${err}`))
        }
    }, [personId, trajectoryId])

    const mapOptions = useMemo<google.maps.MapOptions>(
        () => ({
            disableDefaultUI: true,
            clickableIcons: true,
            scrollwheel: true,
            scaleControl: true,
            zoomControl: true,
        }),
        []
    );

    const { isLoaded } = useLoadScript({
        googleMapsApiKey: process.env.NEXT_PUBLIC_GOOGLE_API_KEY as string,
        libraries: libraries as any,
    });

    if (!isLoaded) {
        return <p>Loading...</p>;
    }

    function onMarkerLoad(marker: Marker) {
        console.log(`marker = ${marker}`)
    }

    function title(waypoint: Waypoint): string {
        return `trajectory = ${waypoint.trajectoryId} timestamp = ${waypoint.timestamp}`
    }

    function icon(): string {
        return '/pin.png'
    }

    if (personId < 0) {
        return (<div>Select a Person ID</div>)
    } else if (trajectoryId == null) {
        return (<div>Select a Trajectory ID</div>)
    } else {
        return (
            <div >
                <GoogleMap
                    options={mapOptions}
                    zoom={mapData.zoom}
                    center={mapData.mapCenter}
                    mapTypeId={google.maps.MapTypeId.ROADMAP}
                    mapContainerStyle={{ width: '100em', height: '100em' }}
                    onLoad={() => console.log('Map Component Loaded...')}
                >
                    {mapData.waypoints.map((wp) => <Marker
                        key={wp.id}
                        position={{ lat: wp.lat, lng: wp.lng }}
                        clickable={true}
                        title={title(wp)}
                        icon={icon()}
                    />)}
                </GoogleMap>
            </div>
        )
    }
})

export default MapContainer
