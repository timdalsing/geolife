'use client'

import { useEffect, useState } from "react"
import MapContainer from "./map"
import PersonSelect from "@/app/components/PersonSelect"
import { personIdStore } from "@/lib/store/personIdStore"
import TrajectorySelect from "@/app/components/TrajectorySelect"
import { trajectoryIdStore } from "@/lib/store/trajectoryIdStore"

export default function Person() {

    return (
        <div id='person-container' className="m-3">
            <div id='person-title' className="text-xl">Person</div>
            <div className="flex flex-row">
                <PersonSelect />
                <TrajectorySelect personId={personIdStore} />
            </div>
            <div id='person-map-container'>
                <MapContainer personId={personIdStore} trajectoryId={trajectoryIdStore} />
            </div>
        </div>
    )
}