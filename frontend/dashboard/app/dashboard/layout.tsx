'use client'

import React from 'react'
import Nav from './nav'

export default function DashboardLayout({ children }: { children: React.ReactNode }) {
    return (
        <div id='dashboard-layout' className='flex flex-row'>
            <Nav />
            {children}
        </div>
    )
}
