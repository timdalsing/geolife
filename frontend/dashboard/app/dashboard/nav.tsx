import Link from 'next/link';

export default function Nav() {
    return (
        <div id='nav-container' className='flex flex-col self-auto m-3'>
            <div id='nav-title' className='text-lg'>Geolife Dashboard</div>
            <Link href='/dashboard'>
                <div id='nav-home'>Home</div>
            </Link>
            <Link href='/dashboard/person'>
                <div id='nav-person'>Person</div>
            </Link>
            <Link href='/dashboard/realtime'>
                <div id='nav-realtime'>Real-time</div>
            </Link>
        </div>
    )
}